import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { OrderArticleInfo } from 'src/modules/shared/models/order-articles/order-article-info';

@Component({
  selector: 'app-order-article-list',
  templateUrl: './order-article-list.component.html',
  styleUrls: ['./order-article-list.component.scss']
})
export class OrderArticleListComponent implements OnInit {

  @Input() orderArticles: OrderArticleInfo[] = [];
  @Output() delete: EventEmitter<number> = new EventEmitter();
  @Output() deliver: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  deleteItem(item: number) {
    this.delete.emit(item);
  }

  deliverItem(item: number) {
    this.deliver.emit(item);
  }

}
