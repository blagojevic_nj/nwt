import { DataSource } from '@angular/cdk/collections';
import { Component, OnInit} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, ReplaySubject } from 'rxjs';
import PricelistItem from 'src/modules/shared/models/manager/priceListItem';
import { ManagerService } from '../../services/manager.service';
import { ChangePriceDialogComponent } from '../change-price-dialog/change-price-dialog.component';


@Component({
  selector: 'app-pricelist',
  templateUrl: './pricelist.component.html',
  styleUrls: ['./pricelist.component.scss']
})

export class PricelistComponent implements OnInit {
 
  currentPage: number = 0;
  items: PricelistItem[] = [];
  dataSource = new ExampleDataSource(this.items);
  numberOfElements: number = 0;
  searchParam: string = "";
  filterParam: string = "";


  constructor(private managerService:ManagerService, public dialog: MatDialog) {}


  applyFilter(event: Event) {
/*     const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    } */
  }

  ngOnInit(): void {
    this.getPricelistForPage();
  }


  changePage(pageNumber : number) {

    this.currentPage = pageNumber - 1;
    this.getPricelistForPage();
  }

  getPricelistForPage(): void{
    this.managerService
    .getPricelist(this.currentPage, this.searchParam, this.filterParam)
    .subscribe((res) => {
      if(res)
        this.items = [];
        this.numberOfElements = res.totalElements;
        this.items = res.objects;
        this.items = this.items.map(item => {
          item.picture = "http://localhost:8080"+item.picture;
          return item;
        })
        this.dataSource.setData(this.items);
        
    });
    
  }

  search(searchedText : any): void {

    if (searchedText != null) {
      
      this.searchParam = searchedText.target.value;
      this.getPricelistForPage();
    }
  }


  changePrice(id: number, oldPrice: number): void{
    const dialog = this.dialog.open(ChangePriceDialogComponent,
      {data: {id: id, oldPrice:oldPrice}});

      dialog.afterClosed().subscribe(
        res => {
          if(res)
          {
            res.picture = 'http://localhost:8080'+res.picture;
            this.items[this.items.findIndex((item) => item.id === res.id)] = res;
            this.dataSource.setData(this.items);
          }
        }
      )

  }

  
}



class ExampleDataSource extends DataSource<PricelistItem> {
  private _dataStream = new ReplaySubject<PricelistItem[]>();

  constructor(initialData: PricelistItem[]) {
    super();
    this.setData(initialData);
  }

  connect(): Observable<PricelistItem[]> {
    return this._dataStream;
  }

  disconnect() {}

  setData(data: PricelistItem[]) {
    this._dataStream.next(data);
  }
}


