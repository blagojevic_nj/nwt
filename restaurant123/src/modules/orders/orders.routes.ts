import { Routes } from "@angular/router";
import { EmployeeGuard } from "../auth/guards/employee/employee.guard";
import { OrdersBartenderComponent } from "./components/orders-bartender/orders-bartender.component";

export const OrdersRoutes: Routes = [
  {
    path: "",
    pathMatch: "full",
    component: OrdersBartenderComponent,
    canActivate: [EmployeeGuard],
    data: {expectedTypes: "BARTENDER|COOK"}
  },

];
