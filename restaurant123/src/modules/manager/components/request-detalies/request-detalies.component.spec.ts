import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestDetaliesComponent } from './request-detalies.component';

describe('RequestDetaliesComponent', () => {
  let component: RequestDetaliesComponent;
  let fixture: ComponentFixture<RequestDetaliesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestDetaliesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestDetaliesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
