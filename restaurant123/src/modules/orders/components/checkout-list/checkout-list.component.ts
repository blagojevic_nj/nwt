import { Component, Input, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Checkout } from 'src/modules/shared/models/checkout/Checkout';
import { CheckoutArticle } from 'src/modules/shared/models/checkout/CheckoutArticle';
import { OrderService } from '../../service/order.service';

@Component({
  selector: 'app-checkout-list',
  templateUrl: './checkout-list.component.html',
  styleUrls: ['./checkout-list.component.scss']
})
export class CheckoutListComponent implements OnInit {

  @Input() checkout?: Checkout;

  constructor() { }

  ngOnInit(): void {
  }

}
