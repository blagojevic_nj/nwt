export default interface EmployeeBasic {
    firstName: string,
    lastName: string,
    employeeType: string,
    main: boolean
}