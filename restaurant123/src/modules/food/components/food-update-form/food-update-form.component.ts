import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import IngredientInfo from 'src/modules/shared/models/ingredient-info';
import { FoodService } from '../../services/food.service';
import FoodGroup from 'src/modules/shared/models/food/food-group';
import FoodGroups from 'src/modules/shared/models/food/food-group';
import FoodCategory from 'src/modules/shared/models/food/food-category';
import ElementForSelection from 'src/modules/shared/models/selection/elements-for-selection';
import FoodCreateAndUpdate from 'src/modules/shared/models/food/food-create-update';
import { MatDialog } from '@angular/material/dialog';
import { IngredientAddDialogComponent } from 'src/modules/ingredients/components/ingredient-add-dialog/ingredient-add-dialog.component';
import { FoodCategoryAddDialogComponent } from 'src/modules/food-category/components/food-category-add-dialog/food-category-add-dialog.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-food-update-form',
  templateUrl: './food-update-form.component.html',
  styleUrls: ['./food-update-form.component.scss']
})
export class FoodUpdateFormComponent implements OnInit {

  form: FormGroup;
  ingredients: IngredientInfo[] = [];
  ingredientsInAccurateForm: ElementForSelection[] = [];
  placeHolder : string = "Dodati sastojci:";
  inputPlaceHolder : string = "Dodaj novi sastojak..";
  foodGroups : FoodGroup[] = FoodGroups.ALL_FOOD_GROUPS;
  foodCategories : FoodCategory[] = [];
  selectedIngredients: number[] = [];

  initialSelectedIngredients : ElementForSelection[] = [];
  id : number = -1;
  pictureData: any = "no_image"
  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
    private foodService: FoodService,
    public dialog: MatDialog,
    private route: ActivatedRoute, 
  ) { 
      this.form = this.fb.group({
      name: [null, Validators.required],
      picture: [null],
      description: [null, Validators.required],
      prepratationPrice: [null, Validators.required],
      preparationTime: [null, Validators.required],
      selectedFoodGroup: [null, Validators.required],
      selectedFoodCategory: [null, Validators.required],
      ingredients: [[]],
    });
    }

  ngOnInit(): void {

    this.id = this.route.snapshot.params['id'];

    this.foodService
    .getAllFoodCategories()
      .subscribe((res) => {
        if(res) {
          this.foodCategories = res;
        }
      });

    this.foodService
    .getAllIngredients()
      .subscribe((res) => {
        if(res) {
          this.ingredients = res;
          this.ingredientsInAccurateForm = this.mapIngredients(this.ingredients);
        }
      });

      this.foodService.getFoodById(this.id)
    .subscribe((res) => {
      if(res) {
                
        this.form.patchValue({
          name: res.name,
          picture: "",
          description: res.description,
          preparationPrice: res.preparationPrice,
          preparationTime: res.preparationTime,
          selectedFoodGroup: res.foodGroup,
          selectedFoodCategory: res.foodCategory.name,
        });
        
        this.initialSelectedIngredients = this.mapIngredients(res.ingredients);
    }});
    
  }

   submit():void{
     if(this.form.valid){
      const updatedFood: FoodCreateAndUpdate = {
        name: this.form.value.name,
        picture: this.pictureData,
        description: this.form.value.description,
        preparationPrice: this.form.value.prepratationPrice,
        preparationTime: this.form.value.preparationTime,
        ingredientIds: this.selectedIngredients,
        foodGroup: this.form.value.selectedFoodGroup,
        foodCategoryId: this.getSelectedCategoryId(this.form.value.selectedFoodCategory)
      }
      // ovde sad ide slanje zahteva
      console.log(updatedFood);

      this.foodService.sendUpdateFoodRequest(updatedFood, this.id)
          .subscribe((res) => {
            this.toastr.success("Uspesno kreiran zahtev za azuriranje hrane!")
            this.router.navigate(["restaurant/food"])

      });
     } else{
       this.toastr.error("Popunite sva polja validnim podacima!")
     }
  }

  mapIngredients(ingredients : IngredientInfo[]) : ElementForSelection[] {

    return ingredients.map(function(ingredient) {return {"id" : ingredient.id, "name" : ingredient.name}});
  }

  getFoodCategoryNames() : string[] {

    return this.foodCategories.map(function(foodCategory) {return foodCategory.name});
  }

  getSelectedIngredients(elements : ElementForSelection[]) {
    this.selectedIngredients = elements.map(element => element.id);
  }

  getSelectedCategoryId(category : string) : number{
    const element = this.foodCategories.find(element => element.name == category);
    return element ? element.id : -1;
  }

  showModalForCreatingIngredient() {
    const dialogRef = this.dialog.open(IngredientAddDialogComponent);
      
    dialogRef.afterClosed().subscribe(result => {
      // if(result){
      //   this.users = [...this.users, result];
      //   this.dataSource.setData(this.users);
      // }
  });
  }

  showModalForCreatingFoodCategory() {
    const dialogRef = this.dialog.open(FoodCategoryAddDialogComponent);
      
    dialogRef.afterClosed().subscribe(result => {
      // if(result){
      //   this.users = [...this.users, result];
      //   this.dataSource.setData(this.users);
      // }
  });
  }

  readFile = async (input: any) => {
    const filePromise = new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(input.files[0])
      reader.onload = async () => {
        try {
          const response = reader.result
          resolve(response);
        } catch (err) {
          reject(err);
        }
       };
      })
    const file_string = await Promise.resolve(filePromise)
    this.pictureData = file_string
    }
}