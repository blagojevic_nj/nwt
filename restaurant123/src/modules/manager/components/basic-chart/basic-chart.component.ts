import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ManagerService } from '../../services/manager.service';
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexDataLabels,
  ApexStroke,
  ApexYAxis,
  ApexTitleSubtitle,
  ApexLegend
} from "ng-apexcharts";
import TurnoverList from 'src/modules/shared/models/reports/turnovers';

export type ChartOptions = {
  series?: ApexAxisChartSeries | any;
  chart?: ApexChart | any;
  xaxis?: ApexXAxis | any;
  stroke?: ApexStroke | any;
  dataLabels?: ApexDataLabels | any;
  yaxis?: ApexYAxis | any;
  title?: ApexTitleSubtitle | any;
  labels?: string[] | any;
  legend?: ApexLegend | any;
  subtitle?: ApexTitleSubtitle | any;
  colors? : any
};

@Component({
  selector: 'app-basic-chart',
  templateUrl: './basic-chart.component.html',
  styleUrls: ['./basic-chart.component.scss']
})
export class BasicChartComponent implements OnInit {


  @ViewChild("chart") chart!: ChartComponent;
  public chartOptions: Partial<ChartOptions>;

  @Input() data: TurnoverList = {dates:[], values:[]};

  constructor(private managerService:ManagerService) {
    
    this.chartOptions = {
      series: [
        {
          name: "Promet",
          data: []
        }
      ],
      chart: {
        type: "area",
        height: 400,
        zoom: {
          enabled: false
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: "straight"
      },

      title: {
        text: "Izveštaj",
        align: "left"
      },
      subtitle: {
        text: "",
        align: "left"
      },
      labels: [],
      xaxis: {
        type: "datetime"
      },
      yaxis: {
        opposite: true
      },
      legend: {
        horizontalAlign: "left"
      },
      colors: [
        "#FF720D",
        "#FF720D"
      ],
    };
   }

  ngOnInit(): void {
    this.chartOptions.series = [{
          name: "Promet",
          data: this.data.values
        }
      ]
    this.chartOptions.labels = this.data.dates
  }

}
