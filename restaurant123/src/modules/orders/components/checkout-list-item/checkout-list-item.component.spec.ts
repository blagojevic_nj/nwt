import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutListItemComponent } from './checkout-list-item.component';

describe('CheckoutListItemComponent', () => {
  let component: CheckoutListItemComponent;
  let fixture: ComponentFixture<CheckoutListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckoutListItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
