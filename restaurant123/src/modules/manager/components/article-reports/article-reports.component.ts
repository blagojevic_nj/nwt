import { Component, OnInit } from '@angular/core';
import TurnoverList from 'src/modules/shared/models/reports/turnovers';
import { ManagerService } from '../../services/manager.service';

@Component({
  selector: 'app-article-reports',
  templateUrl: './article-reports.component.html',
  styleUrls: ['./article-reports.component.scss']
})
export class ArticleReportsComponent implements OnInit {

  data: TurnoverList|undefined;
  yearly: boolean = false

  constructor(private managerService: ManagerService) {}

  ngOnInit(): void {
    this.getMonthly()
  }

  getMonthly(): void {
    this.managerService.getTotalTurnoverThisMonthReport().subscribe(
      res =>{
        if(res)
        {
          this.data=res
          console.log(this.data)
          this.yearly = false
        }
      }
    )
  }

  getYearly(): void{
    this.managerService.getTotalTurnoverThisYearReport().subscribe(
      res =>{
        if(res)
        {
          this.data=res
          console.log(this.data)
          this.yearly = true
        }
      }
    )
  }

}
