import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeesComponent } from './components/employees/employees.component';
import { ManagerRoutes } from './manager.routes'; 
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../shared/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { PricelistComponent } from './components/pricelist/pricelist.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { ManagerHomePageComponent } from './components/manager-home-page/manager-home-page.component';
import { GraphDailyTargetIncomeComponent } from './components/graph-daily-target-income/graph-daily-target-income.component';
import { NgApexchartsModule } from 'ng-apexcharts';
import { DailyTrendingMenusComponent } from './components/daily-trending-menus/daily-trending-menus.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { GraphOrdersComponent } from './components/graph-orders/graph-orders.component';
import { GraphCustomersComponent } from './components/graph-customers/graph-customers.component';
import { GraphMenuComponent } from './components/graph-menu/graph-menu.component';
import { MostFavoritesItemsComponent } from './components/most-favorites-items/most-favorites-items.component';
import { FoodItemsComponent } from './components/most-favorites-items/food-items/food-items.component';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { RequestsComponent } from './components/requests/requests.component';
import { IncomeReportsComponent } from './components/income-reports/income-reports.component';
import { EmployeeReportsComponent } from './components/employee-reports/employee-reports.component';
import { ArticleReportsComponent } from './components/article-reports/article-reports.component';
import { ChangePriceDialogComponent } from './components/change-price-dialog/change-price-dialog.component';
import { RequestDetaliesComponent } from './components/request-detalies/request-detalies.component';
import { ViewRequestComponent } from './components/view-request/view-request.component';
import { BasicChartComponent } from './components/basic-chart/basic-chart.component';
import { BarChartComponent } from './components/bar-chart/bar-chart.component';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [
    EmployeesComponent,
    PricelistComponent,
    UserProfileComponent,
    ManagerHomePageComponent,
    GraphDailyTargetIncomeComponent,
    DailyTrendingMenusComponent,
    GraphOrdersComponent,
    GraphCustomersComponent,
    GraphMenuComponent,
    MostFavoritesItemsComponent,
    FoodItemsComponent,
    RequestsComponent,
    IncomeReportsComponent,
    EmployeeReportsComponent,
    ArticleReportsComponent,
    ChangePriceDialogComponent,
    RequestDetaliesComponent,
    ViewRequestComponent,
    BasicChartComponent,
    BarChartComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(ManagerRoutes),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgbNavModule,
    NgApexchartsModule,
    PerfectScrollbarModule,
    MatDialogModule,
    ChartsModule,
     

  ],
  providers: [
    {
    provide: MatDialogRef,
    useValue: {}
    },
  ],
  exports:[
    ManagerHomePageComponent,
    ArticleReportsComponent,
    IncomeReportsComponent,
    EmployeeReportsComponent,
    DailyTrendingMenusComponent,
    GraphCustomersComponent,
    GraphDailyTargetIncomeComponent,
    GraphMenuComponent,
    GraphOrdersComponent,
    MostFavoritesItemsComponent,
    
  ]
})
export class ManagerModule { }
