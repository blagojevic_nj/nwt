import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { IngredientAddDialogComponent } from 'src/modules/ingredients/components/ingredient-add-dialog/ingredient-add-dialog.component';
import DrinkCard from 'src/modules/shared/models/drink-card';
import { DrinksService } from '../../services/drinks.service';

@Component({
  selector: 'app-drinks-list',
  templateUrl: './drinks-list.component.html',
  styleUrls: ['./drinks-list.component.scss']
})
export class DrinksListComponent implements OnInit {
  drinks: DrinkCard[] = [];

  numberOfElements: number = 0;
  searchParam: string = "";
  filterParam: string = "";
  currentPage: number = 0;
  constructor(private drinkService: DrinksService, public dialog: MatDialog,) { }

  ngOnInit(): void {
    this.getAllDrinkForPage();
  }

  changePage(pageNumber : number) {

    this.currentPage = pageNumber - 1;
    this.getAllDrinkForPage();
  }

  getAllDrinkForPage() {

    this.drinkService
      .getAll(this.currentPage, this.searchParam, this.filterParam)
      .subscribe((res) => {
        if(res) {
          this.drinks = [];
          this.numberOfElements = res.totalElements;
          this.drinks = res.objects;
        }
      });
  }

  openAddIngredientDialog(): void {
    const dialogRef = this.dialog.open(IngredientAddDialogComponent);
      
      dialogRef.afterClosed().subscribe(result => {
        // if(result){
        //   this.users = [...this.users, result];
        //   this.dataSource.setData(this.users);
        // }
    });
  }

  selectedCategory(newCategory : string) {
    
    if (newCategory === undefined) {
      this.filterParam = "";
    } else {
      this.filterParam = newCategory;
    }
    this.getAllDrinkForPage();
  }

  search(searchedText : any) {

    if (searchedText != null) {
      
      this.searchParam = searchedText.target.value;
      this.getAllDrinkForPage();
    }
  }

  isMain(): boolean {
    const empl = sessionStorage.getItem('employee');
    if(empl)
    {
      const employee = JSON.parse(empl);
      return employee.main;
    }
    else{
      return false;
    }
  }
}
