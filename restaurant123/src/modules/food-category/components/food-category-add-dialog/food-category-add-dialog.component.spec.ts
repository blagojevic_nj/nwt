import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodCategoryAddDialogComponent } from './food-category-add-dialog.component';

describe('FoodCategoryAddDialogComponent', () => {
  let component: FoodCategoryAddDialogComponent;
  let fixture: ComponentFixture<FoodCategoryAddDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoodCategoryAddDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodCategoryAddDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
