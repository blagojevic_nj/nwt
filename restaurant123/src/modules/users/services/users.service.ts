import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpResponse,
} from "@angular/common/http";

import { Observable } from "rxjs";
import { UserAll } from 'src/modules/shared/models/userAll';
import { environment } from 'src/environments/environment';
import AdministrativeUserAdd from 'src/modules/shared/models/administrative-user-create';
import AdministrativeUser from 'src/modules/shared/models/administrative-user';
import AdministrativeUserUpdate from 'src/modules/shared/models/administrative-user-update';
import EmployeeUpdate from 'src/modules/shared/models/employee-update';
import Employee from 'src/modules/shared/models/employees/employee';
import ChangePassword from 'src/modules/shared/models/change-password';
import PageOfElements from 'src/modules/shared/models/page/page-of-elements';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private headers = new HttpHeaders({ "Content-Type": "application/json" });

  constructor(private http: HttpClient) { }

  getAll(pageIndex : number, searchParam : string, filterParam : string): Observable<PageOfElements> {
    let queryParams = {};

    return this.http.get<PageOfElements>(this.getRequestUrlForGetAll(pageIndex, searchParam, filterParam), {headers: this.headers});
  }

  private getRequestUrlForGetAll(pageIndex : number, searchParam : string, filterParam : string) : string {
    return environment.API_URL+"/api/user?" + this.appendPaginationParameters(pageIndex) + "&" + 
        this.appendSearchParameters(searchParam, filterParam);
  }

  private appendPaginationParameters(pageIndex : number) : string {
    return "page=" + pageIndex;
  }

  private appendSearchParameters(searchParam : string, filterParam : string) : string {
    return "searchParam=" + searchParam;
  }

  addUser(user: AdministrativeUserAdd): Observable<UserAll> {
    let queryParams = {};

    return this.http.post<UserAll>(environment.API_URL+"/api/administrative-user", user, {
      headers: this.headers,
      responseType: "json",
    });
  }

  updateAdministrativeUser(user: AdministrativeUserUpdate): Observable<UserAll> {
    return this.http.put<UserAll>(environment.API_URL+"/api/administrative-user/"+user.id, user, {
      headers: this.headers,
      responseType: "json",
    });
  }

  updateEmployee(user: EmployeeUpdate): Observable<UserAll> {
    return this.http.put<UserAll>(environment.API_URL+"/api/employee/"+user.id, user, {
      headers: this.headers,
      responseType: "json",
    });
  }

  deleteUser(id: number): Observable<boolean> {
    return this.http.delete<boolean>(environment.API_URL+"/api/user/"+id);
  }

  getUserById(id:number): Observable<UserAll> {
    return this.http.get<AdministrativeUser>(environment.API_URL + "/api/user/"+id);
  }

  getEmployeeById(id:number): Observable<Employee>{
    return this.http.get<Employee>(environment.API_URL + "/api/employee/"+id);
  }

  changePassword(changePassword: ChangePassword) : Observable<boolean>{
    return this.http.post<boolean>(environment.API_URL + "/api/administrative-user/change-password", changePassword);
  }

  getUserByUsername(username: string): Observable<AdministrativeUser> {
    return this.http.get<AdministrativeUser>(environment.API_URL+"/api/administrative-user/my-profile/"+username);
  }
}
