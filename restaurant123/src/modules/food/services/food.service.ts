import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import FoodCategory from 'src/modules/shared/models/food/food-category';
import FoodCreateAndUpdate from 'src/modules/shared/models/food/food-create-update';
import FoodInfo from 'src/modules/shared/models/food/food-info';
import FoodInfoDto from 'src/modules/shared/models/food/food-info-dto';
import IngredientInfo from 'src/modules/shared/models/ingredient-info';
import PageOfElements from 'src/modules/shared/models/page/page-of-elements';

@Injectable({
  providedIn: 'root'
})
export class FoodService {
  private headers = new HttpHeaders({ "Content-Type": "application/json", "employee-pin":"0001"});
  constructor(private http: HttpClient) { }

  getAll(pageIndex : number, searchParam : string, filterParam : string): Observable<PageOfElements> {
    return this.http.get<PageOfElements>(this.getRequestUrlForGetAll(pageIndex, searchParam, filterParam), {headers: this.headers});
  }

  private getRequestUrlForGetAll(pageIndex : number, searchParam : string, filterParam : string) : string {
    return environment.API_URL+"/api/food?" + this.appendPaginationParameters(pageIndex) + "&" + 
        this.appendSearchParameters(searchParam, filterParam);
  }

  private appendPaginationParameters(pageIndex : number) : string {
    return "page=" + pageIndex;
  }

  private appendSearchParameters(searchParam : string, filterParam : string) : string {
    return "searchParam=" + searchParam + "&foodGroup=" + filterParam;
  }

  getFoodById(id: number): Observable<FoodInfoDto> {
    return this.http.get<FoodInfoDto>(environment.API_URL+"/api/food/"+id, {headers: this.headers});
  }

  getAllFoodCategories() : Observable<FoodCategory[]> {
    return this.http.get<FoodCategory[]>(environment.API_URL+"/api/food-category", {headers: this.headers});
  }

  getAllIngredients() : Observable<IngredientInfo[]>{
    return this.http.get<IngredientInfo[]>(environment.API_URL+"/api/ingredient", {headers: this.headers});
  }

  sendCreateFoodRequest(food : FoodCreateAndUpdate) : Observable<any> {
    return this.http.post(environment.API_URL+"/api/food-request/create", food, {
      headers: this.headers,
      responseType: "json",
    });
  }

  sendUpdateFoodRequest(food : FoodCreateAndUpdate, updatingFoodId : number) : Observable<any> {
    return this.http.post(environment.API_URL+"/api/food-request/update", {"foodId" : updatingFoodId, "updatedFood" : food}, {
      headers: this.headers,
      responseType: "json",
    });
  }

  sendDeleteFoodRequest(foodId : number) : Observable<any> {
    return this.http.post(environment.API_URL+"/api/food-request/delete", { "foodId" : foodId}, {
      headers: this.headers,
      responseType: "json",
    });
  }
}