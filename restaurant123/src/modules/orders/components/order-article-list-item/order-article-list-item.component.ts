import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { OrderArticleInfo } from 'src/modules/shared/models/order-articles/order-article-info';

@Component({
  selector: 'app-order-article-list-item',
  templateUrl: './order-article-list-item.component.html',
  styleUrls: ['./order-article-list-item.component.scss']
})
export class OrderArticleListItemComponent implements OnInit {

  @Input() orderArticle!: OrderArticleInfo;
  @Output() delete: EventEmitter<number> = new EventEmitter();
  @Output() deliver: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  deleteItem() {
    this.delete.emit(this.orderArticle.id);
  }

  deliverItem() {
    this.deliver.emit(this.orderArticle.id);
  }
}
