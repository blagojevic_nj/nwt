import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

   @Input() collectionSize: number;
  @Output() pageChanged: EventEmitter<number>;
  currentPage: number;
  
  constructor() { 
    this.collectionSize = 0;
    this.currentPage = 1;
    this.pageChanged = new EventEmitter()
  }

  onPageChange(newPage : number) {
    this.currentPage = newPage;
    this.pageChanged.emit(newPage);
  }

  ngOnInit(): void {
  }


}
