import { CheckoutArticle } from "./CheckoutArticle";

export interface Checkout {
    id: number,
    totalPrice: number,
    note: string,
    checkoutArticles: CheckoutArticle[]
}