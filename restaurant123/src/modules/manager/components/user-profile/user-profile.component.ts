import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { ManagerService } from '../../services/manager.service';
import UserDetalis from 'src/modules/shared/models/manager/userDetalis';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  constructor(private modalService: NgbModal, private routeParam: ActivatedRoute, private managerService: ManagerService, private toStr : ToastrService, private router :Router) {}
  
  
  user: UserDetalis= {
    id: 0,
    contactPhone :"064000000",
    firstName :"Ime",
    lastName :"Prezime",
    email :"posta@mail.com",
    salary: "0.000",
    userType :"COOK",
    pin:'0001'
  };
  imageSrc = 'assets/images/profile/cook.png';
  sub:any;
  id: any = "1";
  pin: string='0000'
  disableGlavni : boolean = true;
  vanRadnogOdnosa: boolean = false;
  isMain: Boolean = false;
  


  uslov=1
  uslovIme : boolean = false;
  uslovPrz : boolean = false;
  uslovMail : boolean = false;
  uslovTelefon : boolean = false;
  uslovPlata : boolean = false;
  uslovPin:  boolean = false;
  uslovpinZauzet: boolean =false;

  
  sizeClass = "";

  ngOnInit(): void {
    this.sub=this.routeParam.paramMap.subscribe(params => { 
       this.id = params.get('id'); 
       this.managerService.getUserDetalies(Number(this.id)).subscribe(res =>
        {
          this.user=res;
          this.user.userType==='Kuvar' ? this.imageSrc='assets/images/profile/cook.png' : this.user.userType==='Konobar' ? this.imageSrc='assets/images/profile/waiter.png' : this.imageSrc='assets/images/profile/bartender.png'
          this.pin=this.user.pin
          this.disableGlavni = this.user.userType ==="Konobar"
          this.vanRadnogOdnosa = this.user.salary === "0.0"

          if(this.user.userType==='Kuvar' || this.user.userType==='Šanker')
          {
            this.managerService.checkIsMain(this.user).subscribe(
              res => 
              {
                this.isMain = res
              }
            )
          }
        }
        );
       
   });
  }
  
  onClick(imagename:any):void {
    this.imageSrc = "assets/images/product/"+imagename;
  }
  
	open(content:any):void {
		this.modalService.open(content);
	}
    
    toggleSizeClass(size:any):void {
        this.sizeClass = size;
    }

    changeName(nameVal: string):void
    {
      if(!nameVal.trim())
      {
        this.uslovIme = true;
        return
      }
      this.uslovIme = false;
      this.user.firstName = nameVal;
    }

    changeSurname(surVal: string):void
    {
      if(!surVal.trim())
      {
        this.uslovPrz = true;
        return
      }
      this.uslovPrz = false;
      this.user.lastName = surVal;
      
    }

    changeMail(mail: string):void
    {
      if(!mail.trim().match("^[^@\\s]+@[^@\\s]+\.[^@\\s]+$"))
      {
        this.uslovMail = true;
        return
      }
      this.uslovMail = false;
      this.user.email = mail;
    }

    changeTel(tel: string):void
    {
      if(!tel.trim().match("^(\\+)?([ 0-9]){6,16}$"))
      {
        this.uslovTelefon = true;
        return
      }
      this.uslovTelefon = false;
      this.user.contactPhone = tel;
      
    }
    changeSal(sal: string):void
    {
      if(!sal.trim().match("^([ 0-9])*.?([ 0-9]){1,2}?$"))
      {
        this.uslovPlata = true;
        return
      }
      this.uslovPlata = false;
      this.user.salary = sal;
      
    }

    changePin(pin: string) :void{
      this.uslovpinZauzet=false;
      if(!pin.trim().match("^([ 0-9]){4}$"))
      {
        this.uslovPin = true;
        return
      }
      this.uslovPin = false;
      if(this.pin === pin.trim())
      return
      this.managerService.checkForPin(pin).subscribe(
        res => {
          if(res)
          {
            this.pin = pin;
          }
          else{
            this.uslovpinZauzet=true;
          }
        }
      )

      
    }

    submit():void
    {
      this.managerService.changeUserInfo(this.user).subscribe(
        res => {
          if(res)
          {
            
          }
        }
      )

      this.managerService.changeUserSalary(this.user).subscribe(
        res => {
          if(res)
          {
            this.toStr.success("Uspešno sačuvane izmene.")
            this.redirect()
          }
        }
      )
    }

    angazuj():void
    {
      if(Number(this.user.salary) && Number(this.user.salary) >=1){
        this.submit()
        this.redirect()
        return
      }
      else{
        this.toStr.error("Plata ne sme biti 0!");
        return
      }
    }

    otpusti():void
    {
      this.managerService.fireEmployee(this.user).subscribe(
        res => {
          if(res)
          {
            this.toStr.success("Uspešno uklonjen zaposleni")
            this.redirect()
          }
        }
      )
    }

    setMain()
    {
      if(this.user.userType==="Kuvar")
      {
        this.managerService.setMainCook(this.user,!this.isMain).subscribe
        (
          res=>
          {
            if(res){
              this.toStr.success("Uspešno postavljena uloga!")
            }
          }
        )
      }
      if(this.user.userType==="Šanker"){
        this.managerService.setMainBartender(this.user,!this.isMain).subscribe
        (
          res=>
          {
            if(res){
              this.toStr.success("Uspešno postavljena uloga!")
            }
          }
        )
      }
      this.redirect()
    }
    redirect():void
    {
      this.router.navigate(["/restaurant/manager/employees"])
    }

}
