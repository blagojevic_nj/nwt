import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { env } from 'process';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PositionedTableElement } from 'src/modules/shared/models/floor-plan/positioned-table-element';
import { tableCode, TableElement } from 'src/modules/shared/models/floor-plan/table-element';

@Injectable({
  providedIn: 'root'
})
export class FloorPlanService {

  tableMenu: TableElement[] = [
    {
        code: 'CHAIR_1',
        capacity: 1,
        width: 4,
        height: 4
    },
    {
        code: "CHAIR_2",
        capacity: 1,
        width: 4,
        height: 4
    },
    {
        code: "POOL_TABLE",
        capacity: 4,
        width: 10,
        height: 10
    },
    {
        code: "SOFA_CORNER",
        capacity: 6,
        width: 12,
        height: 12
    },
    {
        code: "TABLE_FLAT_2",
        capacity: 2,
        width: 10,
        height: 10
    },
    {
        code: "TABLE_RECT_4",
        capacity: 4,
        width: 8,
        height: 8
    },
    {
        code: "TABLE_RECT_8",
        capacity: 8,
        width: 12,
        height: 12
    },
    {
        code: "TABLE_ROUND_4",
        capacity: 4,
        width: 8,
        height: 8
    },
    {
        code: "TABLE_ROUND_5",
        capacity: 5,
        width: 10,
        height: 10
    },
    {
        code: "TABLE_ROUND_8",
        capacity: 8,
        width: 12,
        height: 12
    }
  ]

  private headers = new HttpHeaders({ "Content-Type": "application/json"});

  constructor(private http: HttpClient) { }

  getFloorPlan(): Observable<PositionedTableElement[]> {
    const url = environment.API_URL+"/api/tables/";
    return this.http.get<PositionedTableElement[]>(url, {headers:this.headers})
    .pipe(map(list => { return list.map(
      elem => { return {...elem, tableElement: this.getTableElement(elem.tableCode)}}
    )}));
  }

  setFloorPlan(plan: PositionedTableElement[]) {
    const url = environment.API_URL+"/api/tables/";
    plan.map(plan => {
      plan.id = !!Number(plan.id) ? plan.id : null; 
      plan.capacity = plan.tableElement?.capacity
    })
    return this.http.post(url, plan, {responseType: 'text'})
  }

  getTableElements(): TableElement[] {
    return this.tableMenu;
  }

  getTableElement(code: tableCode): TableElement {
    return this.tableMenu.find(table => table.code === code)!;
  }

  occupyTable(tableId: number) {
    const headers = new HttpHeaders({ "employee-pin": this.getPin()});
    const url = `${environment.API_URL}/api/tables/occupy/${tableId}`
    return this.http.put(url, {}, {headers: headers});
  }

  freeTable(tableId: number) {
    const headers = new HttpHeaders({ "employee-pin": this.getPin()});
    const url = `${environment.API_URL}/api/tables/free/${tableId}`
    return this.http.put(url, {}, {headers: headers});
  }

  cancelTable(tableId: number) {
    const headers = new HttpHeaders({ "employee-pin": this.getPin()});
    const url = `${environment.API_URL}/api/orders/${tableId}/cancel-table`
    return this.http.put(url, {}, {headers: headers});
  }


  getPin():string {
    const pin = sessionStorage.getItem("pin");
    if(pin) return pin;
    return '';
  }

}
