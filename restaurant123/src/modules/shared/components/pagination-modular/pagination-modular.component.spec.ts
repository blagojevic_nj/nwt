import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginationModularComponent } from './pagination-modular.component';

describe('PaginationModularComponent', () => {
  let component: PaginationModularComponent;
  let fixture: ComponentFixture<PaginationModularComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaginationModularComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginationModularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
