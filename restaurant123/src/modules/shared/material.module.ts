import { NgModule } from '@angular/core';

import {MatCardModule} from '@angular/material/card'
import { MatInputModule } from '@angular/material/input';
import {MatButtonModule } from '@angular/material/button'
import {MatSidenavModule} from '@angular/material/sidenav'
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import { MatTableModule } from '@angular/material/table'
import {MatMenuModule} from '@angular/material/menu';
import {MatDialogModule} from '@angular/material/dialog';
import {MatChipsModule} from '@angular/material/chips';
import {MatTooltipModule} from '@angular/material/tooltip';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatOptionModule } from '@angular/material/core';
import { DragDropModule } from '@angular/cdk/drag-drop';
import {MatExpansionModule} from '@angular/material/expansion';
import { PaginationComponent } from './components/pagination/pagination.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PaginationModularComponent } from './components/pagination-modular/pagination-modular.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AutocompleteMultipleSelectComponent } from './components/autocomplete-multiple-select/autocomplete-multiple-select.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { CommonModule } from '@angular/common';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';

const modules = [
  MatCardModule,
  MatInputModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatFormFieldModule,
  MatSelectModule,
  MatTableModule,
  MatMenuModule,
  MatDialogModule,
  MatChipsModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatSortModule,
  MatOptionModule,
  DragDropModule,
  MatExpansionModule,
  NgbModule,
  FormsModule,
  ReactiveFormsModule,
  MatAutocompleteModule,
  CommonModule
];

@NgModule({
  imports: modules,
  exports: [modules, PaginationComponent, AutocompleteMultipleSelectComponent, PaginationModularComponent],
  declarations: [
    PaginationComponent,
    AutocompleteMultipleSelectComponent,
    PaginationModularComponent,
    ConfirmDialogComponent
  ],
})
export class MaterialModule {}