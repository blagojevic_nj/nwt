export default interface OrderArticleStatusUpdate {
    newStatus: string;
}