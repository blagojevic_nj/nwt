import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../shared/material.module';
import { DrinksRoutes } from './drinks.routes';
import { RouterModule } from '@angular/router';
import { DrinkInfoComponent } from './components/drink-info/drink-info.component';
import { DrinksListComponent } from './components/drinks-list/drinks-list.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { IngredientsModule } from '../ingredients/ingredients.module';
import { DrinkCreateFormComponent } from './components/drink-create-form/drink-create-form.component';
import { DrinkUpdateFormComponent } from './components/drink-update-form/drink-update-form.component';

@NgModule({
  declarations: [
    DrinkInfoComponent,
    DrinksListComponent,
    DrinkCreateFormComponent,
    DrinkUpdateFormComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild(DrinksRoutes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    IngredientsModule,
  ]
})
export class DrinksModule { }
