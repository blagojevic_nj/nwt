import OrderArticle from "../order-articles/employee-order-article";

export default interface ActiveOrder {
    id: number,
    startTime: number,
    orderStatus: string,
    note: string,
    orderArticles: OrderArticle[]
};