import { Component, OnInit } from '@angular/core';
import HomepageDateValuePair from 'src/modules/shared/models/reports/HomepageDateValuePair';
import HomepageStatistics from 'src/modules/shared/models/reports/HomepageStatistics';
import { ManagerService } from '../../services/manager.service';

@Component({
  selector: 'app-manager-home-page',
  templateUrl: './manager-home-page.component.html',
  styleUrls: ['./manager-home-page.component.scss']
})
export class ManagerHomePageComponent implements OnInit {

  orderCount : HomepageDateValuePair[] = [{date:new Date(),value:0}];

  constructor(private managerService:ManagerService) {

    this.managerService.getHomepageOrderStats()
    .subscribe(res =>{
      if(res){
      }
    })
   }

  ngOnInit(): void {
  }

      

}
