import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import DrinkInfo from 'src/modules/shared/models/drink-info';
import { DrinksService } from '../../services/drinks.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/modules/shared/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-drink-info',
  templateUrl: './drink-info.component.html',
  styleUrls: ['./drink-info.component.scss']
})
export class DrinkInfoComponent implements OnInit {

  id: number = -1;
  drink: DrinkInfo;

  constructor(
    private route:ActivatedRoute, 
    private drinkService: DrinksService, 
    private router:Router,
    private toast:ToastrService,
    private dialog: MatDialog) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.drinkService.getOne(this.id).subscribe((result) => {
      this.drink = result;
    },
    (error) => {
      this.router.navigate(["/page-not-found"])
    });
  }

  deleteDrink(): void{
    const dialogRef = this.dialog.open(ConfirmDialogComponent);
      
      dialogRef.afterClosed().subscribe(result => {
        if(result){
          this.drinkService.deleteDrink(this.drink.id).subscribe((success) => {
          this.toast.success("Uspešno kreiran zahtev za brisanje!");
          },
          (error) => {
            this.toast.error("Greška, pokušajte ponovo!");
          })
        }
    });
  }

  updateDrink(): void{
    this.router.navigate(["/restaurant/drinks/form/"+this.id]);
  }

  isMain(): boolean {
    const empl = sessionStorage.getItem('employee');
    if(empl)
    {
      const employee = JSON.parse(empl);
      return employee.main;
    }
    else{
      return false;
    }
  }

}
