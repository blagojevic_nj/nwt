import { OrderArticleCreate } from "../order-articles/order-article-create";

export default interface OrderInfo {
    id: number,
    startTime: number,
    endTime: number,
    orderStatus: string,
    totalPrice: number | null,
    note: string,
    orderArticles: OrderArticleCreate[]
};