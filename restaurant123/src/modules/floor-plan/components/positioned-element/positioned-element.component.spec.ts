import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionedElementComponent } from './positioned-element.component';

describe('PositionedElementComponent', () => {
  let component: PositionedElementComponent;
  let fixture: ComponentFixture<PositionedElementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PositionedElementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionedElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
