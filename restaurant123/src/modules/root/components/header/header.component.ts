import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { NgbDropdownConfig} from '@ng-bootstrap/ng-bootstrap'
import { ToastrService } from 'ngx-toastr';
import { ChangePasswordDialogComponent } from 'src/modules/users/components/change-password-dialog/change-password-dialog.component';
import { SharedService } from '../../services/shared.service';
import { InputPinDialogComponent } from '../input-pin-dialog/input-pin-dialog.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [NgbDropdownConfig]
})
export class HeaderComponent implements OnInit {

  toggleChat: boolean = false;
  name: string = "";
  type: string = "";
  imgSrc: string = "";
  administrative: boolean = false;
  constructor(public dialog: MatDialog, private sharedService: SharedService,
    private router:Router, private toast: ToastrService) { }

  ngOnInit(): void {
    const empl = sessionStorage.getItem('employee');
    const user = sessionStorage.getItem('user');
    if(empl){
      const employee = JSON.parse(empl);
      this.name = employee.firstName + " " + employee.lastName;
      switch(employee.employeeType){
        case "COOK": {
          if(employee.main) this.type = "Glavni kuvar";
          else this.type = "Kuvar";

          this.imgSrc = "assets/images/profile/cook-profile.png";
          break;
        }
        case "BARTENDER": {
          if(employee.main) this.type = "Glavni sanker";
          else this.type = "Sanker";
          this.imgSrc = "assets/images/profile/bartender-profile.png";
          break;
        }
        case "WAITER": {
          this.type = "Konobar";
          this.imgSrc = "assets/images/profile/waiter-profile.png";
          break;
        }
      }
    } else if(user){
      this.administrative = true;
      const jwt: JwtHelperService = new JwtHelperService();
      const info = jwt.decodeToken(user);
      this.name = info.sub;
      switch(info.role){
        case "ADMIN": {
          this.type = "Admin sistema";

          this.imgSrc = "assets/images/profile/administrative-user.png";
          break;
        }
        case "OWNER": {
          this.type="Vlasnik"
          this.imgSrc = "assets/images/profile/administrative-user.png";
          break;
        }
        case "MANAGER": {
          this.type = "Menadzer";
          this.imgSrc = "assets/images/profile/administrative-user.png";
          break;
        }
      }
    }
  }

  togglechatbar() {
		this.toggleChat = !this.toggleChat;
	}

  changePin(): void{
    sessionStorage.removeItem('employee');
    sessionStorage.removeItem("pin");
    sessionStorage.removeItem("userRole");
    this.router.navigate(["/restaurant/panels"])
  }

  setEmployee(pin: string):void {
    this.sharedService.getByPin(pin).subscribe(
      (result) => {
        sessionStorage.setItem('employee', JSON.stringify(result));
        sessionStorage.setItem("pin", pin);
        this.toast.success("Dobrodosli, "+ result.firstName);
        if(result.employeeType == "BARTENDER"){
          this.router.navigate(["/restaurant/orders"])
          location.reload();
        }
      },
      (error) => {
        this.toast.error("Pin nije validan!");
      }
    );
  }

  openChangeDialog() : void{
    this.dialog.open(ChangePasswordDialogComponent);
  }
}
