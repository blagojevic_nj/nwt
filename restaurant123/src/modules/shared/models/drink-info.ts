import IngredientInfo from "./ingredient-info";

export default interface DrinkInfo {
    id: number;
    name: string,
    picture: string,
    description: string,
    preparationPrice: number,
    price: number,
    alcohol: boolean,
    ingredients: IngredientInfo[]
}