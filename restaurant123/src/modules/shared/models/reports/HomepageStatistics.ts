import HomepageDateValuePair from "./HomepageDateValuePair";

export default interface HomepageStatistics{
    list:HomepageDateValuePair[]
}