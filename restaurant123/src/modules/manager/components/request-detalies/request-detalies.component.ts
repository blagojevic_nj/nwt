import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import Request from 'src/modules/shared/models/request';
import { ManagerService } from '../../services/manager.service';

@Component({
  selector: 'app-request-detalies',
  templateUrl: './request-detalies.component.html',
  styleUrls: ['./request-detalies.component.scss']
})
export class RequestDetaliesComponent implements OnInit {

  foodRequests: boolean = true;
  requestId:any = "1";
  requestType:String|null = 'food'
  requestTypeIsUpdate : Boolean = false
  oldArticle : Request|undefined
  newArticle : Request|undefined


  constructor(private routeParam: ActivatedRoute, private managerService: ManagerService, private toStr : ToastrService, private router :Router) { }

  ngOnInit(): void {
    document.body.style.backgroundSize="90%";
    this.routeParam.paramMap.subscribe(
      param =>
      {
        this.requestId = param.get('id');
        this.requestType = param.get('type');
        if(this.requestType==="food")
        {
            this.managerService.getFoodRequest(this.requestId).subscribe(
              requests =>{
                if(requests)
                {
                  this.oldArticle = requests[0];
                  if(requests.length > 1)
                  {
                      this.requestTypeIsUpdate = true;
                      this.newArticle = requests[1];
                  }
                }
              }
            )
            
        }else if(this.requestType==="drink")
        {
          this.foodRequests=false
          this.managerService.getDrinkRequest(this.requestId).subscribe(
            requests =>{
              if(requests)
              {
                this.oldArticle = requests[0];
                if(requests.length > 1)
                {
                    this.requestTypeIsUpdate = true;
                    this.oldArticle = requests[1];
                    this.newArticle = requests[0];
                }
              }
            }
          )
        }
        else{
          this.redirect()
        }
      }
    )
  }

  prihvati():void{
    if(this.foodRequests)
    {
      this.managerService.acceptFoodRequest(this.requestId)
      .subscribe(res=>{
        if(res)
        {
          this.toStr.success("Zahtev uspešno prihvaćen!")
          this.redirect()
        }  
        else{
          this.toStr.error("Greška, pokušajte ponovo!")
          this.redirect()
        }
      })
    }
    else{
      this.managerService.acceptDrinkRequest(this.requestId)
      .subscribe(res=>{
        if(res)
        {
          this.toStr.success("Zahtev uspešno prihvaćen!")
          this.redirect()
        }
        else{
          this.toStr.error("Greška, pokušajte ponovo!")
          this.redirect()
        }
      })
      return
    }
    
  }

  odbaci():void{
    if(this.foodRequests)
    {
      
      this.managerService.declineFoodRequest(this.requestId)
      .subscribe(res=>{      
        if(res)
        {
          this.toStr.success("Zahtev uspešno odbijen!")
          this.redirect()
        }
        else{
          this.toStr.error("Greška, pokušajte ponovo!")
          this.redirect()
        }
      })
      
      return
    }
    else{
      
      this.managerService.declineDrinkRequest(this.requestId)
      .subscribe(res=>{
        if(res)
        {
          this.toStr.success("Zahtev uspešno odbijen!")
          this.redirect()
        }
        else{
          this.toStr.error("Greška, pokušajte ponovo!")
          this.redirect()
        }
      })
      this.redirect()
      return
    }
  }

  redirect():void{
    /* window.location.href="/restaurant/manager/requests" */
    this.router.navigate(['/restaurant/manager/requests']);
  }
}
