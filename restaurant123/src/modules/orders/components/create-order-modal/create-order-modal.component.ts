import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { CreateOrder } from 'src/modules/shared/models/orders/create-order';
import { OrderArticleCreate } from 'src/modules/shared/models/order-articles/order-article-create';
import { OrderArticleInfo } from 'src/modules/shared/models/order-articles/order-article-info';
import PricelistItemSearch from 'src/modules/shared/models/pricelist-item/pricelist-item';
import { OrderService } from '../../service/order.service';
import OrderInfo from 'src/modules/shared/models/orders/order-info';

@Component({
  selector: 'app-create-order-modal',
  templateUrl: './create-order-modal.component.html',
  styleUrls: ['./create-order-modal.component.scss']
})
export class CreateOrderModalComponent implements OnInit {

  pricelistItems: PricelistItemSearch[] = [];
  orderArticles: OrderArticleInfo[] = [];
  tableId: number = -1;
  orderId: number = -1;
  mode: "CREATE" | "EDIT" = "CREATE";
  noteText: string = "";

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {mode: "CREATE" | "EDIT", tableId: number}, 
    private fb: FormBuilder,
    private orderService: OrderService,
    private toastr: ToastrService, 
    public matDialogRef: MatDialogRef<CreateOrderModalComponent>) { }

  ngOnInit(): void {
    this.tableId = this.data.tableId;
    this.mode = this.data.mode;
    this.reload();
  }

  updateData(data: PricelistItemSearch[]) {
    this.pricelistItems = data;
  }

  addedItem(item: PricelistItemSearch) {
    let oa: OrderArticleInfo = {
      pricelistItemId: item.id,
      name: item.name,
      price: item.price,
      quantity: item.qty!,
      status: 'PENDING'
    }
    this.orderArticles.push(oa);
    if (this.mode == "EDIT") {
      this.orderService.insertArticle(
      {
        pricelistItemId: item.id,
        quantity: item.qty!
      }, this.orderId).subscribe(
        () => {this.toastr.success("Stavka dodata"); this.reload();},
        () => {this.toastr.error("Došlo je do greške prilikom dodavanja stavke")}
      );
    }
  }

  createOrder() {
    let oa: OrderArticleCreate[] = this.orderArticles.map<OrderArticleCreate>(oa => {return {quantity: oa.quantity, pricelistItemId: oa.pricelistItemId!}})
    let order: CreateOrder = {
      note: this.noteText,
      tablePk: this.tableId,
      orderArticles: oa
    }
    this.orderService.addOrder(order).subscribe(
      () => { 
        this.toastr.success("Porudžbina kreirana");
        this.matDialogRef.close('success');
      },
      () => { this.toastr.error("Došlo je do greške prilikom preuzimanja stola")}
    );
  }

  updateNote() {
    this.orderService.updateNote(this.noteText, this.orderId).subscribe(
      () => {this.toastr.success("Ažurirano")},
      () => {this.toastr.error("Došlo je do greške prilikom ažuriranja")}
    )
  }

  deleteArticle(id: number) {
    if (this.mode === "EDIT") {
      this.orderService.removeArticle(id).subscribe(
        () => {
          this.toastr.success("Obrisano");
          this.reload();
        },
        () => {this.toastr.error("Došlo je do greške prilikom brisanja")}
      )
    } else {
      this.orderArticles = this.orderArticles.filter(oa => oa.id != id);
    }
  }

  deliverArticle(id: number) {
    this.orderService.deliverArticle(id).subscribe(
      () => {
        this.toastr.success("Isporuceno");
        this.reload();
      },
      () => {this.toastr.error("Došlo je do greške prilikom isporuke")}
    )
  }

  reload() {
    this.orderService.getLastOrderForTable(Number(this.tableId) || -1).subscribe(
      (res) => {
        if (!!res) {
          this.orderArticles = [];
          for (let a of res.orderArticles) {
            this.orderService.getPricelistItem(a.pricelistItemId || -1).subscribe((res) => {
              this.orderArticles.push({
                name: res.name,
                price: res.price,
                quantity: a.quantity,
                status: a.status || "",
                id: a.id
              })
            })
          }
          this.orderId = res.id;
          this.noteText = res.note;
      }
    })
  }

}
