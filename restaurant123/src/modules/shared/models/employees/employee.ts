export default interface Employee {
    id: number,
    contactPhone: string,
    firstName: string,
    lastName: string,
    email: string,
    salary: string,
    userType: string
}