import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PositionedTableElement } from 'src/modules/shared/models/floor-plan/positioned-table-element';
import interact from 'interactjs'
import { v4 as uuid } from 'uuid';
import { FloorPlanService } from '../../services/floor-plan.service';
import { tableCode, TableElement } from 'src/modules/shared/models/floor-plan/table-element';
import { MatDialog } from '@angular/material/dialog';
import { TableNumberModalComponent } from '../table-number-modal/table-number-modal.component';
import { ToastrService } from 'ngx-toastr';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-floor-plan',
  templateUrl: './floor-plan.component.html',
  styleUrls: ['./floor-plan.component.scss']
})
export class FloorPlanComponent implements OnInit {

  tables: TableElement[] = new Array();
  gridSize: number = 10;
  scale: number = 1;
  selectedTable: string = '';

  @Input() positionedTables: PositionedTableElement[] = [];
  @Input() mode: 'EDIT' | 'VIEW' | 'PREVIEW' = "EDIT";
  @Output() onTableClick: EventEmitter<PositionedTableElement> = new EventEmitter();

  constructor(public dialog: MatDialog, private floorPlanService: FloorPlanService, private toastr: ToastrService) { }

  
  ngOnInit(): void {
    interact('.draggable')
    .draggable({
      allowFrom: '.handle',
      // only one table can be dragged at the same time
      max: 1,
      // call this function on every dragmove event
      onmove: (event: any) => {
          var target = event.target,
              // keep the dragged position in the data-x/data-y attributes
              x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
              y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

          // translate the element
          target.style.webkitTransform =
          target.style.transform =
              'translate(' + x + 'px, ' + y + 'px)';

          // update the posiion attributes
          target.setAttribute('data-x', x);
          target.setAttribute('data-y', y);
      },
      enabled: this.mode !== "VIEW",
      modifiers: [
        interact.modifiers.restrictRect({
          restriction: '.plan-container'
        }),
        interact.modifiers.snap({
          targets: [
            interact.snappers.grid({ x: this.gridSize, y: this.gridSize })
          ],
          range: Infinity,
          origin: {x: 1, y: -1},
          relativePoints: [ { x: 0, y: 0 } ]
        }),
      ]
    })
    .on('down', (event) => {
      this.selectedTable = event.target.dataset.tableid;
    })

    interact('.floor')
    .dropzone({
      accept: '.draggable',
      overlap: 1,
      ondrop: (event) => {
        const target = event.relatedTarget; 
        const tableId = target.dataset.tableid;
        const moved = this.positionedTables.filter(t => t.id?.toString() === tableId.toString())[0];
        const x = Math.round((parseFloat(target.getAttribute('data-x')) || 0) / this.gridSize);
        const y = Math.round((parseFloat(target.getAttribute('data-y')) || 0) / this.gridSize);
        console.log(moved)
        moved.leftPos = x;
        moved.topPos = y;
      }
    })
    .on('down', (event) => {
      if (event.target.classList.contains('floor')) {
        this.selectedTable = "";
      }
      
    },
    )

    interact('.delete-table')
    .dropzone({
      accept: '.draggable',
      overlap: 0.05,
      ondragenter: (event) => {
        event.relatedTarget.classList.add('table-to-remove');
        event.target.classList.remove('drop-inactive');
        event.target.classList.add('drop-active');
      },
      ondragleave: (event) => {
        event.relatedTarget.classList.remove('table-to-remove');
        event.target.classList.remove('drop-active');
        event.target.classList.add('drop-inactive');
      },
      ondrop: (event) => {
        const tableId = event.relatedTarget.dataset.tableid;
        event.target.classList.remove('drop-active');
        event.target.classList.add('drop-inactive');
        this.positionedTables = this.positionedTables.filter(t => t.id?.toString() !== tableId.toString());
      }
    })

    this.tables = this.floorPlanService.getTableElements();
  }

  availableTableClick(event: any) {
    const tableCode = event.target.dataset.tableid;
    const id = uuid();
    const positionedTable: PositionedTableElement = {
      id: id,
      tableNumber: 0,
      tableCode: tableCode,
      tableElement: this.floorPlanService.getTableElement(tableCode),
      status: "FREE",
      leftPos: 0,
      topPos: 0,
      rotation: 0    
    }
    this.positionedTables.push(positionedTable);
    this.selectedTable = id;
  }

  rotateTable(event: any) {
    if (this.mode !== "EDIT") {
      return;
    }
    const tableId: string = event.target.dataset.tableid;
    const positionedTableElement = this.positionedTables.filter(t => t.id?.toString() === tableId.toString())[0];
    const newRotation = positionedTableElement.rotation + 90;
    const correctedRotation = newRotation - Math.floor(newRotation / 360) * 360;
    positionedTableElement.rotation = correctedRotation;
    const tableElement = positionedTableElement.tableElement!;
    const w = tableElement.width;
    const h = tableElement.height;
    tableElement.width = h;
    tableElement.height = w;
  }

  onPositionedTableClick(table: PositionedTableElement) {
    this.onTableClick.emit(table);
  }
  
  toggleTableAvailability(table: PositionedTableElement) {
    table.status === "UNAVAILABLE" ? table.status = "FREE" : table.status = "UNAVAILABLE";
  }

  gridChanged(event: any) {
    const checked = event.target.checked;
    this.mode = checked ? 'EDIT' : 'PREVIEW';
  }

  getTableElement(code: tableCode) {
    return this.floorPlanService.getTableElement(code);
  }

  showNumberModal(table: PositionedTableElement) {
    this.dialog.open(TableNumberModalComponent, {data: {
      table: table
    }});
  }

  savePlan() {
    this.floorPlanService.setFloorPlan(this.positionedTables).subscribe(
      res => { 
        console.log(res);
        this.toastr.success("Uspešno promenjen raspored"); 
        this.floorPlanService.getFloorPlan().subscribe(res => this.positionedTables = res);
      }, 
      err => {
        console.log(err);
        this.toastr.error("Greška prilikom menjanja rasporeda. Stolovi moraju biti slobodni kada se menjaju!")}
    );
    
  }

  resetPlan() {
    this.floorPlanService.getFloorPlan().subscribe(res => this.positionedTables = res);
  }

}
