import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { MaterialModule } from '../shared/material.module';
import { FoodCategoryAddDialogComponent } from './components/food-category-add-dialog/food-category-add-dialog.component';

@NgModule({
  declarations: [
    FoodCategoryAddDialogComponent
  ],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgbDropdownModule,
    MaterialModule
  ]
})
export class FoodCategoryModule { }
