import { Component, OnInit } from '@angular/core';
import FoodCard from 'src/modules/shared/models/food/food-card';
import FoodGroup from 'src/modules/shared/models/food/food-group';
import FoodGroups from 'src/modules/shared/models/food/food-group';
import { FoodService } from '../../services/food.service';

@Component({
  selector: 'app-food-list',
  templateUrl: './food-list.component.html',
  styleUrls: ['./food-list.component.scss'],
})
export class FoodListComponent implements OnInit {

  foodList: FoodCard[] = [];
  numberOfElements: number = 0;
  searchParam: string = "";
  filterParam: string = "";
  currentPage: number = 0;
  foodGroups: FoodGroup[] = FoodGroups.ALL_FOOD_GROUPS;
  constructor(private foodService: FoodService) { }

  ngOnInit(): void {
    this.getAllFoodForPage();
  }

  changePage(pageNumber : number) {

    this.currentPage = pageNumber - 1;
    this.getAllFoodForPage();
  }

  getAllFoodForPage() {

    this.foodService
      .getAll(this.currentPage, this.searchParam, this.filterParam)
      .subscribe((res) => {
        if(res) {
          this.foodList = [];
          this.numberOfElements = res.totalElements;
          res.objects.forEach( (element) => {
            this.foodList.push(new FoodCard(element.id, element.name, element.picture, element
              .foodGroup))
          })
        }
      });
  }

  selectedCategory(newCategory : string) {
    
    if (newCategory === undefined) {
      this.filterParam = "";
    } else {
      this.filterParam = newCategory;
    }
    this.getAllFoodForPage();
  }

  search(searchedText : any) {

    if (searchedText != null) {
      
      this.searchParam = searchedText.target.value;
      this.getAllFoodForPage();
    }
  }
}
