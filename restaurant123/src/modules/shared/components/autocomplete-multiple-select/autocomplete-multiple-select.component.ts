import {COMMA, E, ENTER} from '@angular/cdk/keycodes';
import { Component, OnInit, ElementRef, ViewChild, Input, Output } from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatAutocompleteSelectedEvent, MatAutocomplete} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import ElementForSelection from '../../models/selection/elements-for-selection';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-autocomplete-multiple-select',
  templateUrl: './autocomplete-multiple-select.component.html',
  styleUrls: ['./autocomplete-multiple-select.component.scss']
})
export class AutocompleteMultipleSelectComponent implements OnInit {

  ngOnInit(): void {

  }

  ngOnChanges() : void {

    this.initialSelectedElements.forEach(element => {

      if (element) {
        this.elements.push(element);
      }
  
      this.formInput.nativeElement.value = '';
      this.formControl.setValue(null);
      this.ingredientListChanged.emit(this.elements);
    })
  }

  visible = true;
  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  formControl = new FormControl();

  @Input() placeHolder: string = "";
  @Input() allElements: ElementForSelection[] = [];
  @Input() initialSelectedElements: ElementForSelection[] = [];
  @Output() elements: ElementForSelection[] = [];
  @Input() inputPlaceHolder: string = ""
  filteredElements: Observable<ElementForSelection[]>;

  @Output() ingredientListChanged : EventEmitter<ElementForSelection[]>;

  @ViewChild('formInput') formInput!: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete!: MatAutocomplete;

  constructor() {
    this.filteredElements = this.formControl.valueChanges.pipe(
        startWith(null),
        map((element: string | null) => element ? this._filter(element) : this.allElements.slice()));
    this.ingredientListChanged = new EventEmitter();
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      const element = this.findElementByName(value.trim());
      if (element)
        this.elements.push(element);
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.formControl.setValue(null);
  }

  findElementByName(name : string) : ElementForSelection | undefined {

    return this.allElements.find(element => element.name === name);
  }

  remove(element: ElementForSelection): void {
    const index = this.elements.indexOf(element);

    if (index >= 0) {
      this.elements.splice(index, 1);
    }
    this.ingredientListChanged.emit(this.elements);
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    const element = this.findElementByName(event.option.value);
    if (element) {
      this.elements.push(element);
    }

    this.formInput.nativeElement.value = '';
    this.formControl.setValue(null);
    this.ingredientListChanged.emit(this.elements);
  }

  private _filter(value: string): ElementForSelection[] {
    const filterValue = value.toLowerCase();

    return this.allElements.filter(element => element.name.toLowerCase().indexOf(filterValue) === 0);
  }
}
