import FoodCardInterface from "../food/food-card";

export default interface PageOfElements {

    objects : any[];
    totalElements : number;
}
