import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeePanelsComponent } from './employee-panels.component';

describe('EmployeePanelsComponent', () => {
  let component: EmployeePanelsComponent;
  let fixture: ComponentFixture<EmployeePanelsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeePanelsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeePanelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
