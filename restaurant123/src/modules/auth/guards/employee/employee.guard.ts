import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class EmployeeGuard implements CanActivate {

  constructor(private router: Router){}

  canActivate(route: ActivatedRouteSnapshot): boolean{

    const expectedTypes: string = route.data.expectedTypes;
    const empl = sessionStorage.getItem('employee');
    
    if(!sessionStorage.getItem("activated"))
    {
      this.router.navigate(["/restaurant/auth/activate"])
      return false;
    }

    if(empl){
      const employee = JSON.parse(empl);
      const types: string[] = expectedTypes.split("|", 2);

      if (types.indexOf(employee.employeeType) === -1) {
        this.router.navigate(["/restaurant/employee-panels"]);
        return false;
      } else{
        return true;
      }
    } else 
    {
      return false;
    }
  }
 
}
