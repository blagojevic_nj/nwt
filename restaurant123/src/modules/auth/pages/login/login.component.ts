import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ToastrService } from 'ngx-toastr';
import { Login } from 'src/modules/shared/models/login';
import { AuthService } from '../../services/auth-service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService
    ) {
    this.form = this.fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required],
    });
   }

  ngOnInit(): void {
  }

  submit() {
    if(this.form.valid){
      const auth: Login = {
      username: this.form.value.username,
      password: this.form.value.password,
    };

    this.authService.login(auth).subscribe(
      (result) => {
        this.toastr.success("Uspesna prijava!");
        sessionStorage.setItem("user", JSON.stringify(result.accessToken));
        const jwt: JwtHelperService = new JwtHelperService();
        const role: string =jwt.decodeToken(result.accessToken).role.toLowerCase();
        if(role === "admin")
          this.router.navigate(["restaurant/users"]);
          else if(role === "manager")
          this.router.navigate(["restaurant/manager"]);
        else if(role === "owner")
          this.router.navigate(["restaurant/manager"]);
      },
      (error) => {
        this.toastr.error("Uneti kredencijali nisu validni!");
      }
    );
    }
    else{
      this.toastr.error("Popunite sva polja!");
    }
  }
}
