import { F } from '@angular/cdk/keycodes';
import { Component, OnInit } from '@angular/core';
import TurnoverList from 'src/modules/shared/models/reports/turnovers';
import { ManagerService } from '../../services/manager.service';

@Component({
  selector: 'app-income-reports',
  templateUrl: './income-reports.component.html',
  styleUrls: ['./income-reports.component.scss']
})
export class IncomeReportsComponent implements OnInit {

  data : TurnoverList|undefined
  monthly: Boolean = true
  dailyReport: TurnoverList|undefined
  monthlyReport: TurnoverList|undefined

  constructor(private managerService: ManagerService) {}

  ngOnInit(): void {
    this.managerService.getProfitsDaily().subscribe(res => {
      if(res){
        this.dailyReport=res
        console.log("daily\n")
        console.log(this.dailyReport)
        this.data = this.dailyReport
      }
    })

    this.managerService.getProfitsMonthly().subscribe(res => {
      if(res){
        this.monthlyReport=res
        console.log("monthly\n")
        console.log(this.monthlyReport)
      }
    })
  }


  getMonthly(): void {
    this.data = this.monthlyReport;
    this.monthly = false;
  }

  getDaily(): void{
    this.data = this.dailyReport;
    this.monthly = true;
  }

}
