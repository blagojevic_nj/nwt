import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import PricelistItemSearch from 'src/modules/shared/models/pricelist-item/pricelist-item';

@Component({
  selector: 'app-order-article-result-list',
  templateUrl: './order-article-result-list.component.html',
  styleUrls: ['./order-article-result-list.component.scss']
})
export class OrderArticleResultListComponent implements OnInit {

  @Input() pricelistItems!: PricelistItemSearch[];
  @Output() added: EventEmitter<PricelistItemSearch> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  addedItem(item: PricelistItemSearch) {
    this.added.emit(item);
  }

}
