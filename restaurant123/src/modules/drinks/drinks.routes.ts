import { Routes } from "@angular/router";
import { EmployeeGuard } from "../auth/guards/employee/employee.guard";
import { DrinkInfoComponent } from "./components/drink-info/drink-info.component";
import { DrinksListComponent } from "./components/drinks-list/drinks-list.component";
import { DrinkCreateFormComponent } from "./components/drink-create-form/drink-create-form.component";
import { DrinkUpdateFormComponent } from "./components/drink-update-form/drink-update-form.component";

export const DrinksRoutes: Routes = [
  {
    path: "",
    pathMatch: "full",
    component: DrinksListComponent,
    canActivate: [EmployeeGuard],
    data: {expectedTypes: "BARTENDER"}
  },
  {
    path: "form",
    pathMatch: "full",
    component: DrinkCreateFormComponent,
    canActivate: [EmployeeGuard],
    data: {expectedTypes: "BARTENDER"}
  },
  {
    path: "form/:id",
    pathMatch: "full",
    component: DrinkUpdateFormComponent,
    canActivate: [EmployeeGuard],
    data: {expectedTypes: "BARTENDER"}
  },
  {
    path: ":id",
    pathMatch: "full",
    component: DrinkInfoComponent,
    canActivate: [EmployeeGuard],
    data: {expectedTypes: "BARTENDER"}
  },
  
];
