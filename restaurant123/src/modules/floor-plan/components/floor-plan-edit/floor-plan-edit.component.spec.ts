import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FloorPlanEditComponent } from './floor-plan-edit.component';

describe('FloorPlanEditComponent', () => {
  let component: FloorPlanEditComponent;
  let fixture: ComponentFixture<FloorPlanEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FloorPlanEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FloorPlanEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
