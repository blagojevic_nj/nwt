import { Component, Input, OnInit } from '@angular/core';
import { PositionedTableElement } from 'src/modules/shared/models/floor-plan/positioned-table-element';
import { TableElement } from 'src/modules/shared/models/floor-plan/table-element';
import { FloorPlanService } from '../../services/floor-plan.service';

@Component({
  selector: 'app-positioned-element',
  templateUrl: './positioned-element.component.html',
  styleUrls: ['./positioned-element.component.scss']
})
export class PositionedElementComponent implements OnInit {

  @Input() table!: PositionedTableElement;

  constructor(private floorPlanService: FloorPlanService) { }

  ngOnInit(): void {
  }

}
