import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnerArticlesReportComponent } from './owner-articles-report.component';

describe('OwnerArticlesReportComponent', () => {
  let component: OwnerArticlesReportComponent;
  let fixture: ComponentFixture<OwnerArticlesReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OwnerArticlesReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnerArticlesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
