import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import FoodInfo from 'src/modules/shared/models/food/food-info'
import { FoodService } from '../../services/food.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/modules/shared/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-food-info',
  templateUrl: './food-info.component.html',
  styleUrls: ['./food-info.component.scss']
})
export class FoodInfoComponent implements OnInit {

  id : number = -1;
  foodInfo : FoodInfo = {
    id: -1,
    name: "",
    picture: "",
    description: "",
    preparationPrice: -1,
    preparationTime: -1,
    price: -1,
    ingredients: [],
    foodGroup: {groupEnumValue : "-1" , groupName : ""},
    foodCategory: {name : "", id : -1, description : ""}
  };
  
  constructor(
    private route: ActivatedRoute, 
    private foodService: FoodService,
    private router: Router,
    private toast:ToastrService,
    private dialog: MatDialog) {}

  ngOnInit() : void {
    this.id = this.route.snapshot.params['id'];
    this.foodService.getFoodById(this.id)
    .subscribe((res) => {
      if (res) {
        this.foodInfo = new FoodInfo(res);
      }
    });
  }

  updateFood() : void {
    this.router.navigate(['/restaurant/food/form/' + this.id]);
  }

  deleteFood() : void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent);
      
      dialogRef.afterClosed().subscribe(result => {
        if(result){
         this.foodService.sendDeleteFoodRequest(this.id)
        .subscribe((res) => {
          this.toast.success("Uspešno kreiran zahtev za brisanje!");
          this.router.navigate(["restaurant/food"])
    })
        }
    });
  }
}
