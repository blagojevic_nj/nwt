import DrinkInfo from "../drink-info";

export default interface DrinkRequest {
    requestType: string,
    id: number,
    drink: DrinkInfo,
    drinkToChange: DrinkInfo
}