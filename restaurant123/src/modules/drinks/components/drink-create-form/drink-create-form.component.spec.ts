import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrinkCreateFormComponent } from './drink-create-form.component';

describe('DrinkCreateFormComponent', () => {
  let component: DrinkCreateFormComponent;
  let fixture: ComponentFixture<DrinkCreateFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrinkCreateFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrinkCreateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
