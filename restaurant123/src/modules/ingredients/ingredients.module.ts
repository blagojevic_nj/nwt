import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IngredientAddDialogComponent } from './components/ingredient-add-dialog/ingredient-add-dialog.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { MaterialModule } from '../shared/material.module';

@NgModule({
  declarations: [
    IngredientAddDialogComponent
  ],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgbDropdownModule,
    MaterialModule
  ]
})
export class IngredientsModule { }
