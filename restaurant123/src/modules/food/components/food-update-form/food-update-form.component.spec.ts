import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodUpdateFormComponent } from './food-update-form.component';

describe('FoodUpdateFormComponent', () => {
  let component: FoodUpdateFormComponent;
  let fixture: ComponentFixture<FoodUpdateFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoodUpdateFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodUpdateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
