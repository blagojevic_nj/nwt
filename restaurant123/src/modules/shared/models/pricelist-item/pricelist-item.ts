import { Allergen } from "../allergen/allergen";

export default interface PricelistItemSearch {
    id: number,
    name: string,
    picture: string,
    description: number,
    price: number,
    allergens: Allergen[],
    qty?: number
}