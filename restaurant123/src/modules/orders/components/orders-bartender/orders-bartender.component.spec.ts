import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersBartenderComponent } from './orders-bartender.component';

describe('OrdersBartenderComponent', () => {
  let component: OrdersBartenderComponent;
  let fixture: ComponentFixture<OrdersBartenderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdersBartenderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersBartenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
