import IngredientInfo from "../ingredient-info";

export default interface DrinkUpdate {
    name: string,
    picture: string,
    description: string,
    preparationPrice: number,
    ingredients: IngredientInfo[],
    alcohol: boolean,
    drinkUpdatedId: number,
    id: any
}