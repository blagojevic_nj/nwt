import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import IngredientInfo from 'src/modules/shared/models/ingredient-info';
import { FoodService } from '../../services/food.service';
import FoodGroup from 'src/modules/shared/models/food/food-group';
import FoodGroups from 'src/modules/shared/models/food/food-group';
import FoodCategory from 'src/modules/shared/models/food/food-category';
import ElementForSelection from 'src/modules/shared/models/selection/elements-for-selection';
import FoodCreateAndUpdate from 'src/modules/shared/models/food/food-create-update';
import { MatDialog } from '@angular/material/dialog';
import { IngredientAddDialogComponent } from 'src/modules/ingredients/components/ingredient-add-dialog/ingredient-add-dialog.component';
import { FoodCategoryAddDialogComponent } from 'src/modules/food-category/components/food-category-add-dialog/food-category-add-dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-food-create-form',
  templateUrl: './food-create-form.component.html',
  styleUrls: ['./food-create-form.component.scss']
})
export class FoodCreateFormComponent implements OnInit {

  form: FormGroup;
  ingredients: IngredientInfo[] = [];
  ingredientsInAccurateForm: ElementForSelection[] = [];
  placeHolder : string = "Dodati sastojci:";
  inputPlaceHolder : string = "Dodaj novi sastojak..";
  foodGroups : FoodGroup[] = FoodGroups.ALL_FOOD_GROUPS;
  foodCategories : FoodCategory[] = [];
  selectedIngredients: number[] = [];
  pictureData: any = "no_image";

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
    private foodService: FoodService,
    public dialog: MatDialog
    ) { 
      this.form = this.fb.group({
      name: [null, Validators.required],
      picture: [null],
      description: [null, Validators.required],
      prepratationPrice: [null, Validators.required],
      preparationTime: [null, Validators.required],
      selectedFoodGroup: [null, Validators.required],
      selectedFoodCategory: [null, Validators.required],
      ingredients: [[]],
    });
    }

  ngOnInit(): void {

    this.foodService
    .getAllFoodCategories()
      .subscribe((res) => {
        if(res) {
          this.foodCategories = res;
        }
      });

    this.foodService
    .getAllIngredients()
      .subscribe((res) => {
        if(res) {
          this.ingredients = res;
          this.ingredientsInAccurateForm = this.mapIngredients();
        }
      });
  }

   submit():void{
     if(this.form.valid){
      const newFood: FoodCreateAndUpdate = {
        name: this.form.value.name,
        picture: this.pictureData,
        description: this.form.value.description,
        preparationPrice: this.form.value.prepratationPrice,
        preparationTime: this.form.value.preparationTime,
        ingredientIds: this.selectedIngredients,
        foodGroup: this.form.value.selectedFoodGroup,
        foodCategoryId: this.getSelectedCategoryId(this.form.value.selectedFoodCategory)
      }
      // ovde sad ide slanje zahteva
      console.log(newFood);

      this.foodService.sendCreateFoodRequest(newFood)
          .subscribe((res) => {
              this.toastr.success("Uspesno kreiran zahtev za novu hranu!")
              this.router.navigate(["restaurant/food"])

      });
     } else{
       this.toastr.error("Popunite sva polja validnim podacima!")
     }
  }

  mapIngredients() : ElementForSelection[] {

    return this.ingredients.map(function(ingredient) {return {"id" : ingredient.id, "name" : ingredient.name}});
  }

  getFoodCategoryNames() : string[] {

    return this.foodCategories.map(function(foodCategory) {return foodCategory.name});
  }

  getSelectedIngredients(elements : ElementForSelection[]) {
    this.selectedIngredients = elements.map(element => element.id);
  }

  getSelectedCategoryId(category : string) : number{
    const element = this.foodCategories.find(element => element.name == category);
    return element ? element.id : -1;
  }

  showModalForCreatingIngredient() {

    const dialogRef = this.dialog.open(IngredientAddDialogComponent);

    dialogRef.afterClosed().subscribe(() => {
      this.ngOnInit()
  });
  }

  showModalForCreatingFoodCategory() {

    const dialogRef = this.dialog.open(FoodCategoryAddDialogComponent);
      
    dialogRef.afterClosed().subscribe(() => {
      this.ngOnInit()
  });
  }

  readFile = async (input: any) => {
    const filePromise = new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(input.files[0])
      reader.onload = async () => {
        try {
          const response = reader.result
          resolve(response);
        } catch (err) {
          reject(err);
        }
       };
      })
    const file_string = await Promise.resolve(filePromise)
    this.pictureData = file_string
    }
}