import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocompleteMultipleSelectComponent } from './autocomplete-multiple-select.component';

describe('AutocompleteMultipleSelectComponent', () => {
  let component: AutocompleteMultipleSelectComponent;
  let fixture: ComponentFixture<AutocompleteMultipleSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutocompleteMultipleSelectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteMultipleSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
