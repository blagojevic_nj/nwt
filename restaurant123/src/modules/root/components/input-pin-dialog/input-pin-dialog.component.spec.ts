import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputPinDialogComponent } from './input-pin-dialog.component';

describe('InputPinDialogComponent', () => {
  let component: InputPinDialogComponent;
  let fixture: ComponentFixture<InputPinDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputPinDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputPinDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
