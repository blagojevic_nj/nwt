import { Component, Input, OnInit } from '@angular/core';
import { TableElement } from 'src/modules/shared/models/floor-plan/table-element';

@Component({
  selector: 'app-menu-element',
  templateUrl: './menu-element.component.html',
  styleUrls: ['./menu-element.component.scss']
})
export class MenuElementComponent implements OnInit {

  @Input() table!: TableElement;

  constructor() { }

  ngOnInit(): void {
  }

}
