export default interface TopArticle {
    id: number,
    picture: string,
    name: string, 
    count: number
}