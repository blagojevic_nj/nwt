import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionedElementWrapperComponent } from './positioned-element-wrapper.component';

describe('PositionedElementWrapperComponent', () => {
  let component: PositionedElementWrapperComponent;
  let fixture: ComponentFixture<PositionedElementWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PositionedElementWrapperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionedElementWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
