import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AuthModule } from '../auth/auth.module';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { CommonModule } from "@angular/common";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NotFoundPageComponent } from './pages/not-found-page/not-found-page.component';
import { RootLayoutComponent } from './pages/root-layout/root-layout.component';
import { ToastrModule } from 'ngx-toastr';
import { MaterialModule } from '../shared/material.module';
import { UsersModule } from '../users/users.module';
import { Interceptor } from '../shared/interceptors/interceptor.interceptor';
import { DrinksModule } from '../drinks/drinks.module';
import { EmployeePanelsComponent } from './components/employee-panels/employee-panels.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { NavHeaderComponent } from './components/nav-header/nav-header.component';
import { HeaderComponent } from './components/header/header.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { IngredientsModule } from '../ingredients/ingredients.module';
import { InputPinDialogComponent } from './components/input-pin-dialog/input-pin-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FoodModule } from '../food/food.module';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { OrdersModule } from '../orders/orders.module';
import { FoodCategoryModule } from '../food-category/food-category.module'
import { FloorPlanModule } from '../floor-plan/floor-plan.module';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundPageComponent,
    RootLayoutComponent,
    EmployeePanelsComponent,
    NavigationComponent,
    NavHeaderComponent,
    HeaderComponent,
    InputPinDialogComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    AuthModule,
    AppRoutingModule,
    ToastrModule.forRoot(),
    MaterialModule,
    UsersModule,
    HttpClientModule,
    DrinksModule,
    NgbModule,
    IngredientsModule,
    FormsModule,
    ReactiveFormsModule,
    FoodModule,
    NgbPaginationModule,
    OrdersModule,
    FoodCategoryModule,
    FloorPlanModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
