import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderArticleResultListItemComponent } from './order-article-result-list-item.component';

describe('OrderArticleResultListItemComponent', () => {
  let component: OrderArticleResultListItemComponent;
  let fixture: ComponentFixture<OrderArticleResultListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderArticleResultListItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderArticleResultListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
