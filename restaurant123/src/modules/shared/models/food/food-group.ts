export default interface  FoodGroup {
    groupName : string,
    groupEnumValue : string
}

export default class FoodGroups{
    public static ALL_FOOD_GROUPS : FoodGroup[] = [
        {"groupEnumValue": "MAIN_DISH", "groupName": "GLAVNO JELO"},
        {"groupEnumValue": "SALAD", "groupName": "SALATA"},
        {"groupEnumValue": "APPETIZER", "groupName": "PREDJELO"},
        {"groupEnumValue": "DESERT", "groupName": "DESERT"}
      ];
} 