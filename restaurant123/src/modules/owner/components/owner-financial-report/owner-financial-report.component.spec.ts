import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnerFinancialReportComponent } from './owner-financial-report.component';

describe('OwnerFinancialReportComponent', () => {
  let component: OwnerFinancialReportComponent;
  let fixture: ComponentFixture<OwnerFinancialReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OwnerFinancialReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnerFinancialReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
