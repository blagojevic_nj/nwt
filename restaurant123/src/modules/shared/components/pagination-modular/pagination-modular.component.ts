import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pagination-modular',
  templateUrl: './pagination-modular.component.html',
  styleUrls: ['./pagination-modular.component.scss']
})
export class PaginationModularComponent implements OnInit {

  @Input() collectionSize: number;
  @Input() pageSize:number;
  @Output() pageChanged: EventEmitter<number>;
  currentPage: number;
  
  constructor() { 
    this.collectionSize = 0;
    this.currentPage = 1;
    this.pageSize = 4;
    this.pageChanged = new EventEmitter()
  }

  onPageChange(newPage : number) {
    this.currentPage = newPage;
    this.pageChanged.emit(newPage);
  }

  ngOnInit(): void {
  }

}
