import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { PositionedTableElement } from 'src/modules/shared/models/floor-plan/positioned-table-element';

@Component({
  selector: 'app-table-number-modal',
  templateUrl: './table-number-modal.component.html',
  styleUrls: ['./table-number-modal.component.scss']
})
export class TableNumberModalComponent implements OnInit {

  form: FormGroup;
  number: number = 100;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {table: PositionedTableElement},
    private fb: FormBuilder,  
    public matDialogRef: MatDialogRef<TableNumberModalComponent>, 
    private toast: ToastrService) 
  { 
    this.form = this.fb.group({
      number: [0, Validators.required]
    });
  }

  ngOnInit(): void {
    this.number = this.data.table.tableNumber;
    this.form.controls['number'].setValue(this.data.table.tableNumber);
  }

  submit(): void{
    if (this.form.valid) {
      this.data.table.tableNumber = this.form.value.number
      this.matDialogRef.close();
      this.toast.success("Sačuvano!")
    } else {
      this.toast.error("Unesite broj između 0 i 1000!")
    }
  }

}
