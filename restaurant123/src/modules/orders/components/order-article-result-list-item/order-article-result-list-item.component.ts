import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import PricelistItemSearch from 'src/modules/shared/models/pricelist-item/pricelist-item';

@Component({
  selector: 'app-order-article-result-list-item',
  templateUrl: './order-article-result-list-item.component.html',
  styleUrls: ['./order-article-result-list-item.component.scss']
})
export class OrderArticleResultListItemComponent implements OnInit {

  @Input() orderArticle!: PricelistItemSearch;
  @Output() added: EventEmitter<PricelistItemSearch> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  panelOpenState = false;

  qty: number = 1;

  addItem() {
    this.added.emit({...this.orderArticle, qty: this.qty});
  }

}
