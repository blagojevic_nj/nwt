import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrinkUpdateFormComponent } from './drink-update-form.component';

describe('DrinkUpdateFormComponent', () => {
  let component: DrinkUpdateFormComponent;
  let fixture: ComponentFixture<DrinkUpdateFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrinkUpdateFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrinkUpdateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
