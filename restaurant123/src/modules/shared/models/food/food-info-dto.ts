import IngredientInfo from "../ingredient-info";
import FoodCategory from "./food-category";

export default interface FoodInfoDto {
    id: number;
    name: string;
    picture: string,
    description: string,
    preparationPrice: number,
    preparationTime: number,
    price: number,
    ingredients: IngredientInfo[],
    foodGroup: string,
    foodCategory: FoodCategory
}
