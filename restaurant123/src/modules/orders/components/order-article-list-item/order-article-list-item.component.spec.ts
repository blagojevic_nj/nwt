import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderArticleListItemComponent } from './order-article-list-item.component';

describe('OrderArticleListItemComponent', () => {
  let component: OrderArticleListItemComponent;
  let fixture: ComponentFixture<OrderArticleListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderArticleListItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderArticleListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
