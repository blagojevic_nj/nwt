import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import DrinkInfo from 'src/modules/shared/models/drink-info';
import DrinkCreate from 'src/modules/shared/models/drink-request/create-request';
import DrinkDeleteRequest from 'src/modules/shared/models/drink-request/delete-request';
import DrinkRequest from 'src/modules/shared/models/drink-request/drink-request';
import DrinkUpdate from 'src/modules/shared/models/drink-request/update-request';
import IngredientInfo from 'src/modules/shared/models/ingredient-info';
import PageOfElements from 'src/modules/shared/models/page/page-of-elements';

@Injectable({
  providedIn: 'root'
})
export class DrinksService {

  file_string: string;
  
  constructor(private http: HttpClient) { }
  
  getAll(pageIndex : number, searchParam : string, filterParam : string): Observable<PageOfElements> {
    const headers = new HttpHeaders({ "Content-Type": "application/json", "employee-pin": this.getPin()});
    return this.http.get<PageOfElements>(this.getRequestUrlForGetAll(pageIndex, searchParam, filterParam), {headers: headers});
  }

  private getRequestUrlForGetAll(pageIndex : number, searchParam : string, filterParam : string) : string {
    const headers = new HttpHeaders({ "Content-Type": "application/json", "employee-pin": this.getPin()});
    return environment.API_URL+"/api/drink?" + this.appendPaginationParameters(pageIndex) + "&" + 
        this.appendSearchParameters(searchParam, filterParam);
  }

  private appendPaginationParameters(pageIndex : number) : string {
    return "page=" + pageIndex;
  }

  private appendSearchParameters(searchParam : string, filterParam : string) : string {
    return "searchParam=" + searchParam + "&alcohol=" + filterParam;
  }

  getOne(id: number): Observable<DrinkInfo> {
    const headers = new HttpHeaders({ "Content-Type": "application/json", "employee-pin": this.getPin()});
    return this.http.get<DrinkInfo>(environment.API_URL+"/api/drink/"+id, {headers: headers});
  }

  deleteDrink(id: number): Observable<DrinkRequest>{
    const deleteDrinkRequest: DrinkDeleteRequest = {
      drinkId: id
    }
    const headers = new HttpHeaders({ "Content-Type": "application/json", "employee-pin": this.getPin()});
    return this.http.delete<DrinkRequest>(environment.API_URL+"/api/drink-request",{headers: headers, body:deleteDrinkRequest})
  }

  getAllIngredients() : Observable<IngredientInfo[]>{
    const headers = new HttpHeaders({ "Content-Type": "application/json", "employee-pin": this.getPin()});
    return this.http.get<IngredientInfo[]>(environment.API_URL+"/api/ingredient", {headers: headers});
  }

  addDrink(newDrink: DrinkCreate) : Observable<DrinkRequest>{
    const headers = new HttpHeaders({ "Content-Type": "application/json", "employee-pin": this.getPin()});
    return this.http.post<DrinkRequest>(environment.API_URL+"/api/drink-request", newDrink, {headers: headers});
  }

  updateDrink(drink: DrinkUpdate): Observable<DrinkRequest> {
    const headers = new HttpHeaders({ "Content-Type": "application/json", "employee-pin": this.getPin()});
    return this.http.put<DrinkRequest>(environment.API_URL+"/api/drink-request", drink, {headers: headers});
  }

  getPin():string {
    const pin = sessionStorage.getItem("pin");
    if(pin) return pin;
    return '';
  }
}
