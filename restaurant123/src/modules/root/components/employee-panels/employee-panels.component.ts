import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from '../../services/shared.service';
import { InputPinDialogComponent } from '../input-pin-dialog/input-pin-dialog.component';

@Component({
  selector: 'app-employee-panels',
  templateUrl: './employee-panels.component.html',
  styleUrls: ['./employee-panels.component.scss']
})
export class EmployeePanelsComponent implements OnInit {

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private sharedService: SharedService,
    private toast: ToastrService
  ) { }

  ngOnInit(): void {
  }

  openInputPin(): void {
    const dialogRef = this.dialog.open(InputPinDialogComponent);
    dialogRef.afterClosed().subscribe(pin => {
        if(pin){
         this.setEmployee(pin);
        }
    });
  }

  setEmployee(pin: string):void {
    this.sharedService.getByPin(pin).subscribe(
      (result) => {
        console.log(result)
        sessionStorage.setItem('employee', JSON.stringify(result));
        sessionStorage.setItem("pin", pin);
        sessionStorage.setItem("userRole", result.employeeType);
        this.toast.success("Dobrodošli, "+ result.firstName);
        if(result.employeeType == "BARTENDER") {
          this.router.navigate(["/restaurant/orders"])
        } else if (result.employeeType == "COOK") {
          this.router.navigate(["/restaurant/orders"])
        } else if (result.employeeType == "WAITER") {
          this.router.navigate(["/restaurant/floor-plan/view"])
        } else {
        }
      },
      (error) => {
        this.toast.error("Pin nije validan!");
      }
    );
  }
}
