import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnerEmployeeReportComponent } from './owner-employee-report.component';

describe('OwnerEmployeeReportComponent', () => {
  let component: OwnerEmployeeReportComponent;
  let fixture: ComponentFixture<OwnerEmployeeReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OwnerEmployeeReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnerEmployeeReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
