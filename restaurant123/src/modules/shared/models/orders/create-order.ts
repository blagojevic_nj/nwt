import { OrderArticleCreate } from "../order-articles/order-article-create";

export interface CreateOrder {
    waiterPk?: number,
    note: string,
    tablePk: number,
    orderArticles: OrderArticleCreate[]
}