export type tableCode = 
"TABLE_RECT_4" 
| "TABLE_RECT_8" 
| "TABLE_ROUND_4" 
| "TABLE_ROUND_5" 
| "TABLE_ROUND_8"
| "CHAIR_1"
| "CHAIR_2"
| "TABLE_FLAT_2"
| "POOL_TABLE"
| "SOFA_CORNER";

export interface TableElement {
    // Koliko ljudi tu moze da sedi
    capacity: number,
    // Na osnovu koda se dobavlja slika
    code: tableCode,
    // Dodeli jedinicne velicine koje ce imati na podu
    width: number,
    height: number
}