export default interface TurnoverList {
    dates: string[],
    values: number[]
}