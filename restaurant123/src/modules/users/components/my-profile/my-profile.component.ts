import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import AdministrativeUser from 'src/modules/shared/models/administrative-user';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements OnInit {

  user: AdministrativeUser;

  constructor(private usersService: UsersService) { }

  ngOnInit(): void {
    this.getUser();
  }

  getUser():void {
    const token = sessionStorage.getItem("user");
    if(token) {
      const jwt = new JwtHelperService();
      const decodedToken = jwt.decodeToken(token);
      const username = decodedToken.sub;

      this.usersService.getUserByUsername(username).subscribe(
        (result) => {
          this.user = result;
        },
        (error) => {

        }
      );

    }
  }

  convertType(): string {
    switch(this.user.userType){
      case "ADMIN" :return "Administrator";
      case "MANAGER": return "Menadžer";
      case "OWNER": return "Vlasnik";
    }
    return "";
  }
}
