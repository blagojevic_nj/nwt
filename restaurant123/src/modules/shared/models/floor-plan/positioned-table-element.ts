import { tableCode, TableElement } from "./table-element";

type tableStatus = 
"FREE"
| "OCCUPIED"
| "PAID"
| "UNAVAILABLE";


export interface PositionedTableElement {
    // Moze ih biti vise pa je ID neophodan (Ovaj id se cuva na beku, zajedno sa podacima iz tableElement.table)
    id: number | string | null,
    // Stavka stola iz menija koja se stavlja na pod
    tableCode: tableCode,
    tableElement?: TableElement,
    // Broj stola (jedinstven)
    tableNumber: number,
    status: tableStatus,
    // Pozicija na podu
    leftPos: number,
    topPos: number,
    rotation: number,
    notification?: boolean
    capacity?: number
}