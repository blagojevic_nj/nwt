import { DataSource } from "@angular/cdk/collections";
import { AfterViewInit, Component, ViewChild, OnInit } from "@angular/core";
import { Observable, ReplaySubject } from "rxjs";
import { UserAll } from "src/modules/shared/models/userAll";
import { UsersService } from "../../services/users.service";
import {MatDialog} from '@angular/material/dialog';
import { ToastrService } from "ngx-toastr";
import { UserAddDialogComponent } from "../user-add-dialog/user-add-dialog.component";
import { UserUpdateDialogComponent } from "../user-update-dialog/user-update-dialog.component";
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap'
@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
  providers: [NgbDropdownConfig]
})
export class UsersListComponent implements OnInit {
  displayedColumns: string[] = ["id", "Ime", "Prezime", "Email", "Plata", "Tip korisnika", "Opcije"];
  users: UserAll[] = [];
  dataSource = new ExampleDataSource(this.users);
  numberOfElements: number = 0;
  searchParam: string = "";
  filterParam: string = "";
  currentPage: number = 0;
  constructor(private usersService: UsersService ,public dialog: MatDialog,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getAllUsersForPage();
  }

  changePage(pageNumber : number) {

    this.currentPage = pageNumber - 1;
    this.getAllUsersForPage();
  }

  getAllUsersForPage(): void {
    this.usersService
      .getAll(this.currentPage, this.searchParam, this.filterParam)
      .subscribe((res) => {
        if(res)
          this.users = [];
          this.numberOfElements = res.totalElements;
          this.users = res.objects;
          this.dataSource.setData(this.users);
      });
  }


  openAddDialog(): void {
    const dialogRef = this.dialog.open(UserAddDialogComponent);
      
      dialogRef.afterClosed().subscribe(result => {
        if(result){
          this.users = [...this.users, result];
          this.dataSource.setData(this.users);
        }
    });
  }

  update(id: number, userType: string):void {
    const dialogRef = this.dialog.open(UserUpdateDialogComponent, {
      data: {id: id, userType: userType}});

    dialogRef.afterClosed().subscribe(result => {
        if(result){
          this.users[this.users.findIndex((user) => user.id === result.id)] = result;
           this.dataSource.setData(this.users);
        }
    });
  }

  delete(id: number):void {
    this.usersService.deleteUser(id).subscribe(
      (result) => {
        this.toastr.success("Uspešno obrisan korisnik!");
        this.users = this.users.filter((user)=>user.id != id);
        this.dataSource.setData(this.users);
      },
      (error) => {
        this.toastr.error(error.error);
      }
    )
  }

  details(id: number):void {
    
  }

  search(searchedText : any) {

    if (searchedText != null) {
      
      this.searchParam = searchedText.target.value;
      this.getAllUsersForPage();
    }
  }

}

class ExampleDataSource extends DataSource<UserAll> {
  private _dataStream = new ReplaySubject<UserAll[]>();

  constructor(initialData: UserAll[]) {
    super();
    this.setData(initialData);
  }

  connect(): Observable<UserAll[]> {
    return this._dataStream;
  }

  disconnect() {}

  setData(data: UserAll[]) {
    this._dataStream.next(data);
  }
}
