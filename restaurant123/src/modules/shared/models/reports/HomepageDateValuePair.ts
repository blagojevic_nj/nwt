export default interface HomepageDateValuePair
{
    date:Date,
    value:number
}