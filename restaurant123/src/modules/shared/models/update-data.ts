export default interface UpdateData {
    id: number,
    userType: string
}