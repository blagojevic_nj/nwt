import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableNumberModalComponent } from './table-number-modal.component';

describe('TableNumberModalComponent', () => {
  let component: TableNumberModalComponent;
  let fixture: ComponentFixture<TableNumberModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableNumberModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableNumberModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
