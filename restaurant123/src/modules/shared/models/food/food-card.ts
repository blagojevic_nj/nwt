export default interface FoodCardInterface {
    id: number;
    name: string;
    picture: string;
    foodGroup: string;
}

export default class FoodCard implements FoodCardInterface{
    id: number;
    name: string;
    picture: string;
    foodGroup: string;

    constructor(id: number, name: string, picture: string, foodGroup: string) {
        this.id = id;
        this.name = name;
        this.picture = this.getPicture(picture);
        this.foodGroup = this.translateFoodGroupName(foodGroup);
    }

    translateFoodGroupName(group: string) : string {

        switch (group) {
            case "MAIN_DISH":
                return "GLAVNO JELO";
            case "SALAD":
                return "SALATA";
            case "APPETIZER":
                return "PREDJELO";
            default:
                return "DESERT";
        }
    }

    getPicture(picture: string) : string {

        if (picture.length === 0) {
            return "assets/images/food-placeholder.png";
        }
        return picture;
    }
}