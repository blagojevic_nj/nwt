import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FloorPlanComponent } from './components/floor-plan/floor-plan.component';
import { RouterModule } from '@angular/router';
import { FloorPlanRoutes } from './floor-plan.routes';
import { FloorPlanViewComponent } from './components/floor-plan-view/floor-plan-view.component';
import { FloorPlanEditComponent } from './components/floor-plan-edit/floor-plan-edit.component';
import { MenuElementComponent } from './components/menu-element/menu-element.component';
import { PositionedElementComponent } from './components/positioned-element/positioned-element.component';
import { PositionedElementWrapperComponent } from './components/positioned-element-wrapper/positioned-element-wrapper.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { TableOptionsComponent } from './components/table-options/table-options.component';
import { TableNumberModalComponent } from './components/table-number-modal/table-number-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';



@NgModule({
  declarations: [
    FloorPlanComponent,
    FloorPlanViewComponent,
    FloorPlanEditComponent,
    MenuElementComponent,
    PositionedElementComponent,
    PositionedElementWrapperComponent,
    TableOptionsComponent,
    TableNumberModalComponent
  ],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    FormsModule,
    ToastrModule,
    ReactiveFormsModule,
    RouterModule.forChild(FloorPlanRoutes)
  ]
})
export class FloorPlanModule { }
