import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { IngredientAddDialogComponent } from 'src/modules/ingredients/components/ingredient-add-dialog/ingredient-add-dialog.component';
import DrinkUpdate from 'src/modules/shared/models/drink-request/update-request';
import IngredientInfo from 'src/modules/shared/models/ingredient-info';
import ElementForSelection from 'src/modules/shared/models/selection/elements-for-selection';
import { DrinksService } from '../../services/drinks.service';

@Component({
  selector: 'app-drink-update-form',
  templateUrl: './drink-update-form.component.html',
  styleUrls: ['./drink-update-form.component.scss']
})
export class DrinkUpdateFormComponent implements OnInit {

  form: FormGroup;
  ingredients: IngredientInfo[] = [];
  ingredientsInAccurateForm: ElementForSelection[] = [];
  placeHolder : string = "Dodati sastojci:";
  inputPlaceHolder : string = "Dodaj novi sastojak..";
  selectedIngredients: IngredientInfo[] = [];
  pictureData: any = "no_image"
  initialSelectedIngredients : ElementForSelection[] = [];
  id : number = -1;
  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private drinkService: DrinksService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.form = this.fb.group({
      name: [null, Validators.required],
      picture: [null],
      description: [null, Validators.required],
      preparationPrice: [null, Validators.required],
      alcohol: [null],
      ingredients: [[]],
   })
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    this.drinkService
    .getAllIngredients()
      .subscribe((res) => {
        if(res) {
          this.ingredients = res;
          this.ingredientsInAccurateForm = this.mapIngredients(this.ingredients);
        }
      });

      this.drinkService.getOne(this.id)
    .subscribe((res) => {
      if(res) {
                
        this.form.patchValue({
          name: res.name,
          picture: "",
          description: res.description,
          preparationPrice: res.preparationPrice,
          alcohol: res.alcohol
        });
        
        this.initialSelectedIngredients = this.mapIngredients(res.ingredients);
    }});
  }

  mapIngredients(ingredients : IngredientInfo[]) : ElementForSelection[] {

    return ingredients.map(function(ingredient) {return {"id" : ingredient.id, "name" : ingredient.name}});
  }
  
  getSelectedIngredients(elements : ElementForSelection[]) {
   this.selectedIngredients = elements.map(element => {
      return this.ingredients.filter((ing) => ing.id === element.id)[0];
    });
  }

  showModalForCreatingIngredient() {
    const dialogRef = this.dialog.open(IngredientAddDialogComponent);
      
    dialogRef.afterClosed().subscribe(result => {
  });
  }

  submit():void{
     if(this.form.valid){
      const updatedFood: DrinkUpdate = {
        name: this.form.value.name,
        picture: this.pictureData,
        description: this.form.value.description,
        preparationPrice: this.form.value.preparationPrice,
        ingredients: this.selectedIngredients,
        alcohol: this.form.value.alcohol,
        drinkUpdatedId: this.id,
        id: null
      }

      this.drinkService.updateDrink(updatedFood)
          .subscribe((res) => {
            if(res) {
              this.toastr.success("Uspesno poslat zahtev!");
              this.router.navigate(["/restaurant/drinks"])
            }

      });
     } else{
       this.toastr.error("Popunite sva polja!")
     }
  }

  readFile = async (input: any) => {
    const filePromise = new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(input.files[0])
      reader.onload = async () => {
        try {
          const response = reader.result
          resolve(response);
        } catch (err) {
          reject(err);
        }
       };
      })
    const file_string = await Promise.resolve(filePromise)
    this.pictureData = file_string
    }
}
