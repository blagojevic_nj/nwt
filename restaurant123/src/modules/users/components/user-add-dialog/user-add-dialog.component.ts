import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import AdministrativeUserAdd from 'src/modules/shared/models/administrative-user-create';
import DialogData from 'src/modules/shared/models/dialog-data';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-user-add-dialog',
  templateUrl: './user-add-dialog.component.html',
  styleUrls: ['./user-add-dialog.component.scss']
})
export class UserAddDialogComponent implements OnInit {

  form: FormGroup;
  constructor( @Inject(MAT_DIALOG_DATA) public data: DialogData, private fb: FormBuilder,private toastr: ToastrService,
  private usersService: UsersService, private router: Router, 
  public matDialogRef: MatDialogRef<UserAddDialogComponent>) {
    this.form = this.fb.group({
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      email: [null, Validators.required],
      username: [null, Validators.required],
      password: [null, Validators.required],
      administrativeUserType: [null, Validators.required],
      contactPhone: [null, Validators.required],
      main: [null, Validators.required],
    });
   }

  ngOnInit(): void {
  }

  submit():void{
    if(this.isAdministrative() && this.checkFormAdministrative())
    {
      const administrativeUserAdd: AdministrativeUserAdd = {
        firstName: this.form.value.firstName,
        lastName: this.form.value.lastName,
        email: this.form.value.email,
        username: this.form.value.username,
        password: this.form.value.password,
        administrativeUserType: this.form.value.administrativeUserType
      }

      this.usersService.addUser(administrativeUserAdd).subscribe(
      (result) => {
        this.toastr.success("Uspešno dodat korisnik!");
        this.matDialogRef.close(result);
      },
      (error) => {
        this.toastr.error(error.error);
      });
    } else if(this.isEmployee() && this.checkFormEmployee()){
      
    } else{
      this.toastr.error("Popunite sva polja!");
    }
  }

  isAdministrative() : boolean {
    return ['ADMIN', 'MANAGER', 'OWNER'].includes(this.form.value.administrativeUserType);
  }

  isEmployee() : boolean {
    return ['COOK', 'WAITER', 'BARTENDER'].includes(this.form.value.administrativeUserType);
  }

  private checkFormAdministrative(): boolean{
    return this.form.value.firstName && this.form.value.lastName && this.form.value.email && this.form.value.username && this.form.value.password;
  }

  private checkFormEmployee(): boolean {
    return this.form.value.firstName && this.form.value.lastName && this.form.value.email && this.form.value.contactPhone && this.form.value.main;
  }
}
