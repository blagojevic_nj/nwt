import { Component, Input, OnInit } from '@angular/core';
import { PositionedTableElement } from 'src/modules/shared/models/floor-plan/positioned-table-element';

@Component({
  selector: 'app-positioned-element-wrapper',
  templateUrl: './positioned-element-wrapper.component.html',
  styleUrls: ['./positioned-element-wrapper.component.scss']
})
export class PositionedElementWrapperComponent implements OnInit {

  @Input() table!: PositionedTableElement;

  constructor() { }

  ngOnInit(): void {
  }
}
