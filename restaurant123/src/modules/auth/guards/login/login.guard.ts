import { Injectable } from '@angular/core';
import { Router, CanActivate} from '@angular/router';
import { AuthService } from '../../services/auth-service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
   constructor(public auth: AuthService, public router: Router) {}
  
  canActivate() : boolean {
    if (this.auth.isLoggedIn()) {
      const role = this.auth.getRole()
      if(role == "admin")
        this.router.navigate(["/restaurant/users"])
      else if(role == "manager")
        this.router.navigate(["/restaurant/manager"])
      else if(role == "owner")
        this.router.navigate(["/restaurant/owner"])
      return false;
    }
    return true;
  }
}
