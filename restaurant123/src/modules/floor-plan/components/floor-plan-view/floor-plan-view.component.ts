import { Component, OnDestroy, OnInit } from '@angular/core';
import { PositionedTableElement } from 'src/modules/shared/models/floor-plan/positioned-table-element';
import { FloorPlanService } from '../../services/floor-plan.service';

import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-floor-plan-view',
  templateUrl: './floor-plan-view.component.html',
  styleUrls: ['./floor-plan-view.component.scss']
})
export class FloorPlanViewComponent implements OnInit, OnDestroy {

  private serverUrl = environment.API_URL + '/socket';
  private stompClient: any;

  isLoaded: boolean = false;
  isCustomSocketOpened = false;

  selectedTable?: PositionedTableElement;
  tables: PositionedTableElement[] = new Array();
  

  constructor(private floorPlanService: FloorPlanService) { }

  ngOnInit(): void {
    this.floorPlanService.getFloorPlan().subscribe(res => this.tables = res);
    this.initializeWebSocketConnection();
  }

  // Funkcija za otvaranje konekcije sa serverom
  initializeWebSocketConnection() {
    // serverUrl je vrednost koju smo definisali u registerStompEndpoints() metodi na serveru
    let ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;

    this.stompClient.connect({}, function () {
      that.isLoaded = true;
      that.openGlobalSocket()
    });
  }
  
  // Funckija za pretplatu na topic /socket-publisher (definise se u configureMessageBroker() metodi)
  // Globalni socket se otvara prilikom inicijalizacije klijentske aplikacije
  openGlobalSocket() {
    if (this.isLoaded) {
      this.stompClient.subscribe('/table-status-changed', (response: any) => {
        this.notifyTable(response);
      });
      }
  }

  notifyTable(msg: any) {
    const tableId = parseInt(msg.body);
    let table = this.tables.filter(t => (Number(t.id) || -1) === tableId)[0];
    console.log(table);
    table.notification = true;
  }

  updateSelectedTable(table: PositionedTableElement) {
    this.selectedTable = table;
  }

  ngOnDestroy(): void {
      
  }

  refreshTables() {
    this.floorPlanService.getFloorPlan().subscribe(
      (res) => {this.tables = res}
    )
  }
  

}
