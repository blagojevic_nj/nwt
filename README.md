# Napredne web tehnologije 2021/22

Predmetni projekat iz predmeta Napredne web tehnologije.

Tim123

## Članovi tima:
- Njegoš Blagojević sw-18-2018
- Dejan Todorović sw-17-2018
- Đorđe Njegić sw-12-2018
- Luka Kureljušić sw-23-2018

## Opis projekta

Projekat predstavlja informacioni sistem namenjen restoranu kojeg će koristiti zaposleni tog restorana a osnovna svrha jeste povećanje efikasnosti rada restorana kao i praćenje svih aktivnosti unutar sistema.

## Pokretanje projekta

Da bi se projekat pokrenuo potrebno je prvo klonirati sam repozitorijum komandom ```git clone https://gitlab.com/blagojevic_nj/nwt ```.

Nakon toga potrebno se pozicionirati na root folder projekta odnostno ukucati komandu ``` cd restaurant123 ```.

Zatim je neophodno instalirati sve potrebne biblioteke, to se može postići komandom ``` npm install ```.

Tokom instaliranja dešavalo se da npm manager odbija da instalira stompjs biblioteku pa je dodatno potrebno pokrenuti komandu ``` npm install stompjs --save ```. Ako se i nakon toga projekat ne bude mogao pokrenuti jer kompajler ne može da pronađe "net" moduo, unesite sledeću komandu ``` npm i net -S ```. 
Na kraju, kada sve uspešno prođe aplikacija se pokreće komandom ``` npm start ``` .


### Kredencijali

- Admin: username:admin; password:123
- Menadžer: username:manager; password:123
- Vlasnik: username:owner; password:123
- Kuvar: pin: 0001(glavni) ili 0002 (obicni)
- Sanker: pin glavni: 0005 ili 0006 (obicni)
- Konobar: pin 0003 ili 0004

Repozitorijum serverske aplikacije se nalazi [ovde](https://gitlab.com/Lule99/kts).
