import { Component, Input, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets, Chart } from 'chart.js';
import { Label } from 'ng2-charts';
import { ManagerService } from '../../services/manager.service';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})


export class BarChartComponent implements OnInit {

  @Input() data :any;


  constructor(private managerService:ManagerService) { }

  barChartOptions: ChartOptions = {
    responsive: false,
    defaultColor: "green",
    scales: {
      yAxes: [{
          ticks: {
              beginAtZero: true
          }
      }]
  }
  };
  barChartLabels: Label[] = ['Kuvari', 'Konobari', 'Sankeri'];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];
  barChartData: ChartDataSets[] = [
    { data: [8, 7, 6],
      label: 'Izveštaj o Zaposlenima',
      backgroundColor:"rgba(255,114,13,1)",
      borderColor: "rgba(194,61,0,1)",
      borderWidth: 2,
      hoverBackgroundColor:'rgba(255,114,13,0.8)',
      hoverBorderColor:'rgba(194,61,0,1)'
     },
  ];

  ngOnInit(): void {
    this.barChartData[0].data = this.data
  }

}
