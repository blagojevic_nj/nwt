import { Routes } from "@angular/router";
import { EmployeeGuard } from "../auth/guards/employee/employee.guard";
import { RoleGuard } from "../auth/guards/role/role.guard";
import { FloorPlanEditComponent } from "./components/floor-plan-edit/floor-plan-edit.component";
import { FloorPlanViewComponent } from "./components/floor-plan-view/floor-plan-view.component";

export const FloorPlanRoutes: Routes = [
    {
        path: "edit",
        pathMatch: "full",
        component: FloorPlanEditComponent,
        canActivate: [RoleGuard],
        data: {expectedRoles: "ADMIN"}
    },
    {
        path: "view",
        pathMatch: "full",
        component: FloorPlanViewComponent,
        canActivate: [EmployeeGuard],
        data: {expectedTypes: "WAITER"}
    }
]