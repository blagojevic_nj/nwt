export default interface Request {
    id: Number,
    type: String,
    name: string,
    preparationPrice: number,
    preparationTime: number,
    picture: string,
    description: string,
    requestType: String,
    price:Number,
    group:String,
    cookName:String

    
}