import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { SharedService } from 'src/modules/root/services/shared.service';

@Component({
  selector: 'app-nav-header',
  templateUrl: './nav-header.component.html',
  styleUrls: ['./nav-header.component.scss']
})
export class NavHeaderComponent implements OnInit {
  hamburgerClass: boolean = false;
  ruta : String = ''
  constructor(private sharedService: SharedService) {
    const token = sessionStorage.getItem("user");
    const jwt: JwtHelperService = new JwtHelperService();
    
    if (token) {
      const info = jwt.decodeToken(token);
      if(info.role === 'MANAGER')
      {
        this.ruta='manager'
      }      
      if(info.role === 'ADMIN')
      {
        this.ruta='users'
      }
      if(info.role === 'OWNER')
      {
        this.ruta='owner'
      }
    }

    
   }

  ngOnInit(): void {
  }

  toggleHamburgerClass(){
	  this.hamburgerClass = this.sharedService.toggleHamburgerClass();
  }

}
