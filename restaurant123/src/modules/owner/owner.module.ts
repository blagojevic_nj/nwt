import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OwnerRoutes } from './owner.routes';
import { OwnerHomePageComponent } from './components/owner-home-page/owner-home-page.component';
import { OwnerFinancialReportComponent } from './components/owner-financial-report/owner-financial-report.component';
import { OwnerEmployeeReportComponent } from './components/owner-employee-report/owner-employee-report.component';
import { OwnerArticlesReportComponent } from './components/owner-articles-report/owner-articles-report.component';
import { ManagerModule } from '../manager/manager.module';
import { RouterModule } from '@angular/router';




@NgModule({
  declarations: [
    OwnerHomePageComponent,
    OwnerFinancialReportComponent,
    OwnerEmployeeReportComponent,
    OwnerArticlesReportComponent,
  ],
  imports: [
    CommonModule,
    ManagerModule,
    RouterModule.forChild(OwnerRoutes),
  ]
})
export class OwnerModule { }
