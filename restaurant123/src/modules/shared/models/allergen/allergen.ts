export type Allergen = 
'MILK'
| 'FISH'
| 'EGGS'
| 'SHELLFISH'
| 'TREE_NUTS'
| 'PEANUTS'
| 'WHEAT'
| 'SOY_BEAN'