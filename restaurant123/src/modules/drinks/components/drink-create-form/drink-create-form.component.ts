import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { IngredientAddDialogComponent } from 'src/modules/ingredients/components/ingredient-add-dialog/ingredient-add-dialog.component';
import DrinkCreate from 'src/modules/shared/models/drink-request/create-request';
import IngredientInfo from 'src/modules/shared/models/ingredient-info';
import ElementForSelection from 'src/modules/shared/models/selection/elements-for-selection';
import { DrinksService } from '../../services/drinks.service';

@Component({
  selector: 'app-drink-create-form',
  templateUrl: './drink-create-form.component.html',
  styleUrls: ['./drink-create-form.component.scss']
})
export class DrinkCreateFormComponent implements OnInit {
  
  form: FormGroup;
  ingredients: IngredientInfo[] = [];
  ingredientsInAccurateForm: ElementForSelection[] = [];
  placeHolder : string = "Dodati sastojci:";
  inputPlaceHolder : string = "Dodaj novi sastojak..";
  selectedIngredients: IngredientInfo[] = [];
  pictureData:any = "no_image";
  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private drinkService: DrinksService,
    public dialog: MatDialog,
    private router: Router
  ) {
    this.form = this.fb.group({
      name: [null, Validators.required],
      picture: [null],
      description: [null, Validators.required],
      prepratationPrice: [null, Validators.required],
      alcohol: false,
      ingredients: [[]],
    });
   }

  ngOnInit(): void {
    this.getAllIngredients();
  }

  getAllIngredients(): void{
    this.drinkService
    .getAllIngredients()
      .subscribe((res) => {
        if(res) {
          this.ingredients = res;
          this.ingredientsInAccurateForm = this.mapIngredients();
        }
      });
  }

  submit():void{
     if(this.form.valid){
      
      const newDrink: DrinkCreate = {
        name: this.form.value.name,
        picture: this.pictureData,
        description: this.form.value.description,
        preparationPrice: this.form.value.prepratationPrice,
        ingredients: this.selectedIngredients,
        alcohol: this.form.value.alcohol
      }
      this.drinkService.addDrink(newDrink).subscribe(
        (result) => {
          this.toastr.success("Uspesno ste poslali zahtev!")
          this.router.navigate(['/restaurant/drinks'])
        },
        (error) => {
          this.toastr.error("Nesto nije u redu!");
        }
      );
     } else{
       this.toastr.error("Popunite sva polja!")
     }
  }

  mapIngredients() : ElementForSelection[] {

    return this.ingredients.map(function(ingredient) {return {"id" : ingredient.id, "name" : ingredient.name}});
  }

  showModalForCreatingIngredient() {
    const dialogRef = this.dialog.open(IngredientAddDialogComponent);
      
    dialogRef.afterClosed().subscribe(result => {
  });
  }

  getSelectedIngredients(elements : ElementForSelection[]) {
    this.selectedIngredients = elements.map(element => {
      return this.ingredients.filter((ing) => ing.id === element.id)[0];
    });
  }

  readFile = async (input: any) => {
    const filePromise = new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(input.files[0])
      reader.onload = async () => {
        try {
          const response = reader.result
          resolve(response);
        } catch (err) {
          reject(err);
        }
       };
      })
    const file_string = await Promise.resolve(filePromise)
    this.pictureData = file_string
    }
}
