export default interface OrderArticle {
    id: number,
    name: string,
    picture: string,
    quatity: number,
    orderArticleStatus: string,
    employee: string,
    parent: number
}