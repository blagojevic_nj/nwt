import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import NewFoodCategory from 'src/modules/shared/models/food/new-food-category';

@Injectable({
  providedIn: 'root'
})
export class FoodCategoryService {
  private headers = new HttpHeaders({ "Content-Type": "application/json", "employee-pin":"0001"});

  constructor(private http: HttpClient) { }

  createNewFoodCategory(foodCategory : NewFoodCategory) : Observable<any> {
    return this.http.post(environment.API_URL+"/api/food-category", foodCategory, {
      headers: this.headers,
      responseType: "json",
    });
  }
}