import { Routes } from "@angular/router";
import { RoleGuard } from "../auth/guards/role/role.guard";
import { MyProfileComponent } from "./components/my-profile/my-profile.component";
import { UsersListComponent } from "./components/users-list/users-list.component";

export const UsersRoutes: Routes = [
    {
        path: "",
        pathMatch: "full",
        component: UsersListComponent,
        canActivate: [RoleGuard],
        data: {expectedRoles: "ADMIN"}
    },
    {
        path: "my-profile",
        pathMatch: "full",
        component: MyProfileComponent,
        canActivate: [RoleGuard],
        data: {expectedRoles: "ADMIN|MANAGER|OWNER"}
    },
];