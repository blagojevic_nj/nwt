export default interface RequestSmall{
    id:number,
    picture:string,
    requestType:string,
    requestCreatorName:string,
    name:string
}