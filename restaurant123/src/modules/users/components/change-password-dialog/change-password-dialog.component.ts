import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ToastrService } from 'ngx-toastr';
import ChangePassword from 'src/modules/shared/models/change-password';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-change-password-dialog',
  templateUrl: './change-password-dialog.component.html',
  styleUrls: ['./change-password-dialog.component.scss']
})
export class ChangePasswordDialogComponent implements OnInit {
  form : FormGroup; 
  constructor(
    private fb: FormBuilder,
    public matDialogRef: MatDialogRef<ChangePasswordDialogComponent>,
    private toast: ToastrService,
    private usersService: UsersService
  ) { 
    this.form = fb.group({
      password: [null, Validators.required],
      newPassword:  [null, Validators.required],
      newPassword2:  [null, Validators.required],
    });
  }

  ngOnInit(): void {
  }

  submit(): void {
    if(this.form.valid && this.form.value.newPassword === this.form.value.newPassword2){
      const token = sessionStorage.getItem('user');
      if(token){
        const jwt: JwtHelperService = new JwtHelperService();
        const username: string = jwt.decodeToken(token).sub; 
        const changePassword: ChangePassword = {
          username:username,
          password: this.form.value.password.trim(),
          newPassword: this.form.value.newPassword.trim()
        }
        this.usersService.changePassword(changePassword).subscribe(
          (result) => {
            this.toast.success("Uspešno promenjena šifra")
            this.matDialogRef.close();
          },
          (error) => {
            this.toast.error("Niste uspeli promeniti!")
          }
        );
      }
    } else{
      this.toast.error("Unesite ispravne podatke!");
    }
  }
}
