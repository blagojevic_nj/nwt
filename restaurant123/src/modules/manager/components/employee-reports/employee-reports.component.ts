import { Component, OnInit } from '@angular/core';
import { flatMap } from 'rxjs/operators';
import { ManagerService } from '../../services/manager.service';

@Component({
  selector: 'app-employee-reports',
  templateUrl: './employee-reports.component.html',
  styleUrls: ['./employee-reports.component.scss']
})
export class EmployeeReportsComponent implements OnInit {

  numberOfEmployees : []|undefined;
  salaries: []|undefined;
  number : Boolean = true
  constructor(private managerService: ManagerService) { }

  ngOnInit(): void {
    this.managerService.getEmployeeNumberReport().subscribe(res=>{
      if(res){
        this.numberOfEmployees = res.data
      }
    })
    this.number = false;
    this.managerService.getEmployeeTotalSalaryReport().subscribe(res=>{
      if(res){
        this.salaries = res.data
      }
    })
  }

  getNumber()
  {
    this.number = true
   
  }

  getSalaries()
  {
    this.number = false;
    
  }

}
