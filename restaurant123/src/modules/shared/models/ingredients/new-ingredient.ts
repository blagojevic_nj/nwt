export default interface NewIngredient {
    name: string,
    allergens: string[]
}