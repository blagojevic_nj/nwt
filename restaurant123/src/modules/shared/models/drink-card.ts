export default interface DrinkCard {
    id: number,
    name: string,
    picture: string,
    price: number,
    alcohol: boolean
}