export default interface IngredientInfo {
    id: number,
    name: string,
    allergens: string[]
}