import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import AdministrativeUserUpdate from 'src/modules/shared/models/administrative-user-update';
import EmployeeUpdate from 'src/modules/shared/models/employee-update';
import UpdateData from 'src/modules/shared/models/update-data';
import { UserAll } from 'src/modules/shared/models/userAll';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-user-update-dialog',
  templateUrl: './user-update-dialog.component.html',
  styleUrls: ['./user-update-dialog.component.scss']
})
export class UserUpdateDialogComponent implements OnInit {
  id: number = -1;
  userType: string = "";
  form: FormGroup;
  user: UserAll;
  contactPhone: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: UpdateData, private fb: FormBuilder,private toastr: ToastrService,
  private usersService: UsersService, private router: Router,
  public matDialogRef: MatDialogRef<UserUpdateDialogComponent>) {
    this.form = this.fb.group({
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      email: [null, Validators.required],
      contactPhone: [null, Validators.required],
    });
   }

  ngOnInit(): void {
    this.id = this.data.id;
    this.userType = this.data.userType;
    if(['ADMIN', 'OWNER', 'MANAGER'].includes(this.userType)){
      this.usersService.getUserById(this.id).subscribe(
      (result) => {
        this.user = result;
        this.form.controls['firstName'].setValue(this.user.firstName);
        this.form.controls['lastName'].setValue(this.user.lastName);
        this.form.controls['email'].setValue(this.user.email);
        this.form.controls['contactPhone'].setValue("sss");
      },
      (error) => {
        this.toastr.error(error.error);
      }
    );
    } else{
      this.usersService.getEmployeeById(this.id).subscribe(
      (result) => {
        this.user = result;
        this.contactPhone = result.contactPhone;
        this.form.controls['firstName'].setValue(this.user.firstName);
        this.form.controls['lastName'].setValue(this.user.lastName);
        this.form.controls['email'].setValue(this.user.email);
        this.form.controls['contactPhone'].setValue(this.contactPhone);
      },
      (error) => {
        this.toastr.error(error.error);
      }
    );
    }
  }

  isEmployee() : boolean{
    return ['WAITER', 'BARTENDER', "COOK"].includes(this.userType);
  }

  submit(): void {
    if(this.form.valid){
      const updatedUser : AdministrativeUserUpdate = {
        id: this.id,
        firstName: this.form.value.firstName,
        lastName: this.form.value.lastName,
        email: this.form.value.email
      };
      this.usersService.getUserById(this.id).subscribe(
        (result) => {
          if(['ADMIN', 'MANAGER', 'OWNER'].includes(result.userType)){
            this.usersService.updateAdministrativeUser(updatedUser).subscribe(
              (result) => {
                this.toastr.success("Uspešno ažurirani podaci!")
                this.matDialogRef.close(result);
              },
              (error) => {

              }
            );
          }else{
            const employee: EmployeeUpdate = {
              id: this.id,
              firstName: this.form.value.firstName,
              lastName: this.form.value.lastName,
              email: this.form.value.email,
              contactPhone: this.form.value.contactPhone,
            };
            this.usersService.updateEmployee(employee).subscribe(
              (result) => {
                this.toastr.success("Uspešno ažurirani podaci!")
                console.log(result)
                this.matDialogRef.close(result);
              },
              (error) => {

              }
            );
          }
        }
      )
    } else{
      this.toastr.error("Unesite ispravne podatke!");
    }
  }
}
