import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Allergen } from 'src/modules/shared/models/allergen/allergen';
import { MenuItemType } from 'src/modules/shared/models/menu-item-type/menu-item-type';
import PricelistItemSearch from 'src/modules/shared/models/pricelist-item/pricelist-item';
import { OrderService } from '../../service/order.service';

interface typeValue {
  value: MenuItemType,
  viewValue: string
}

interface typeAllergen {
  value: Allergen,
  viewValue: string
}

@Component({
  selector: 'app-order-search',
  templateUrl: './order-search.component.html',
  styleUrls: ['./order-search.component.scss']
})
export class OrderSearchComponent implements OnInit {

  @Output() onDataChanged: EventEmitter<PricelistItemSearch[]> = new EventEmitter();

  constructor(private orderService: OrderService) { }

  ngOnInit(): void {
  }

  types: typeValue[] = [
    {value: 'DRINK', viewValue: 'Piće'},
    {value: 'FOOD', viewValue: 'Hrana'},
  ];

  allergens: typeAllergen[] = [
    {value: 'MILK', viewValue: 'Mleko'},
    {value: 'EGGS', viewValue: 'Jaja'},
    {value: 'FISH', viewValue: 'Riba'},
    {value: 'SHELLFISH', viewValue: 'Ljuskar'},
    {value: 'TREE_NUTS', viewValue: 'Orašasti plodovi'},
    {value: 'PEANUTS', viewValue: 'Kikiriki'},
    {value: 'WHEAT', viewValue: 'Žitarice'},
    {value: 'SOY_BEAN', viewValue: 'Soja'}
  ];

  allerg = new FormControl();

  selectedType = this.types[0].value;

  searchValue = '';

  updateSearchParam() {
    this.orderService.searchOrders(this.allerg.value || [], this.searchValue, this.selectedType).subscribe(
      (res) => {this.onDataChanged.emit(res)}
    );
  }
}
