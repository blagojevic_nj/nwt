import { Component, OnInit } from '@angular/core';
import TopArticle from 'src/modules/shared/models/reports/topArticle';
import { ManagerService } from '../../services/manager.service';

@Component({
  selector: 'app-most-favorites-items',
  templateUrl: './most-favorites-items.component.html',
  styleUrls: ['./most-favorites-items.component.scss']
})
export class MostFavoritesItemsComponent implements OnInit {

  topArticles: TopArticle[] = [];

  constructor(private managerService:ManagerService) { }

  ngOnInit(): void {
    this.managerService.topArticlesReport().subscribe((result) => {
      this.topArticles = result
    },
    (error) => {

    })
  } 
}
