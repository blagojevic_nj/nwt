import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import NewIngredient from '../shared/models/ingredients/new-ingredient';

@Injectable({
  providedIn: 'root'
})
export class IngredientService {
  private headers = new HttpHeaders({ "Content-Type": "application/json", "employee-pin":"0001"});

  constructor(private http: HttpClient) { }

  createNewIngredient(ingredient : NewIngredient) : Observable<any> {
    return this.http.post(environment.API_URL+"/api/ingredient", ingredient, {
      headers: this.headers,
      responseType: "json",
    });
  }
}