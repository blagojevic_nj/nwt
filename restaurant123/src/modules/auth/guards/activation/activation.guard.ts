import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ActivationGuard implements CanActivate {
  constructor(public router: Router) {}
  canActivate(): boolean {
    const activated = sessionStorage.getItem('activated');
    
    if(!activated)
      return true;

    const result = JSON.parse(activated);
    if(result === true)
    {
      this.router.navigate(["/restaurant/panels"]);
      return false;
    } 
    return true;
  }
  
}
