import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodCreateFormComponent } from './food-create-form.component';

describe('FoodCreateUpdateFormComponent', () => {
  let component: FoodCreateFormComponent;
  let fixture: ComponentFixture<FoodCreateFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoodCreateFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodCreateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
