export default interface PriceChangeData {
    id: number,
    oldPrice: number,
}