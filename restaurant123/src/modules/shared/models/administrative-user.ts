export default interface AdministrativeUser {
    id: number;
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    salary: string;
    userType: string;

}