export default interface NewFoodCategory {
    name : string,
    description : string;
}