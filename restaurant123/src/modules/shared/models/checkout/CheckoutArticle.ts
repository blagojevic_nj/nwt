export interface CheckoutArticle {
    id: number,
    name: string,
    image: string,
    type: string,
    price: number,
    quantity: number,
    status: string
}