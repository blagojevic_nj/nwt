import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import UserDetalis from 'src/modules/shared/models/manager/userDetalis';
import PageOfElements from 'src/modules/shared/models/page/page-of-elements';
import ChartJs from 'src/modules/shared/models/reports/chartJs';
import HomepageStatistics from 'src/modules/shared/models/reports/HomepageStatistics';
import TopArticle from 'src/modules/shared/models/reports/topArticle';
import TurnoverList from 'src/modules/shared/models/reports/turnovers';
import Request from 'src/modules/shared/models/request';

@Injectable({
  providedIn: 'root'
})
export class ManagerService {

  private headers = new HttpHeaders({ "Content-Type": "application/json"});
  constructor(private http: HttpClient) { }

  getHomepageOrderStats() : Observable<HomepageStatistics>{
    return this.http.get<HomepageStatistics>(environment.API_URL+'/api/reports/order-count-last-7-days-report', {headers:this.headers})
  }

  getPricelist(pageIndex: number, searchParam:string, filterParam:string): Observable<PageOfElements>{
    var url = environment.API_URL+"/api/pricelist?"+"page="+pageIndex+"&searchParam="+searchParam;
    return this.http.get<PageOfElements>(url)
  }

  getDrinkRequestsList(pageIndex: number, searchParam:string):Observable<PageOfElements>{
    var url = environment.API_URL+"/api/drink-request?"+"page="+pageIndex+"&searchParam="+searchParam;
    return this.http.get<PageOfElements>(url)
  }

  getFoodRequestsList(pageIndex: number, searchParam:string):Observable<PageOfElements>{
    var url = environment.API_URL+"/api/food-request?"+"page="+pageIndex+"&searchParam="+searchParam;
    return this.http.get<PageOfElements>(url)
  }

  getAllEmployedList(pageIndex: number, searchParam:string):Observable<PageOfElements>{
    var url = environment.API_URL+"/api/employee/employed?"+"page="+pageIndex+"&searchParam="+searchParam;
    return this.http.get<PageOfElements>(url)
  }

  getAllCooksList(pageIndex: number, searchParam:string):Observable<PageOfElements>{
    var url = environment.API_URL+"/api/employee/cooks?"+"page="+pageIndex+"&searchParam="+searchParam;
    return this.http.get<PageOfElements>(url)
  }

  getAllWaitersList(pageIndex: number, searchParam:string):Observable<PageOfElements>{
    var url = environment.API_URL+"/api/employee/waiters?"+"page="+pageIndex+"&searchParam="+searchParam;
    return this.http.get<PageOfElements>(url)
  }
  
  getAllBartendersList(pageIndex: number, searchParam:string):Observable<PageOfElements>{
    var url = environment.API_URL+"/api/employee/bartenders?"+"page="+pageIndex+"&searchParam="+searchParam;
    return this.http.get<PageOfElements>(url)
  }

  getAllNotEmployedList(pageIndex: number, searchParam:string):Observable<PageOfElements>{
    var url = environment.API_URL+"/api/employee/not-employed?"+"page="+pageIndex+"&searchParam="+searchParam;
    return this.http.get<PageOfElements>(url)
  }

  setNewPrice(pricelistItemId: number, price:number):Observable<PageOfElements>
  {
    var url = environment.API_URL+"/api/pricelist/change-price/"+pricelistItemId+"?price="+price;
    return this.http.put<PageOfElements>(url, {})
  }

  getUserDetalies(id: Number):Observable<UserDetalis>{
    var url = environment.API_URL+"/api/employee/"+id;
    return this.http.get<UserDetalis>(url)

  }

  changeUserInfo(user: UserDetalis):Observable<UserDetalis>
  {
    var url = environment.API_URL+"/api/employee/"+user.id;
    return this.http.put<UserDetalis>(url, user)
  }

  changeUserSalary(user: UserDetalis):Observable<Boolean>
  {
    var url = environment.API_URL+"/api/employee/set-salary/"+user.id+"?salary="+user.salary;
    return this.http.put<Boolean>(url, {})
  }

  checkForPin(pin : string){
  
    var url = environment.API_URL+"/api/employee/check-pin-available?pin="+pin;
    return this.http.get<Boolean>(url)
  }

  fireEmployee(user: UserDetalis):Observable<Boolean>
  { 
    var url = environment.API_URL+"/api/employee/set-salary/"+user.id+"?salary=0";
    return this.http.put<Boolean>(url, {})
  }

  checkIsMain(user: UserDetalis):Observable<Boolean>
  {
    var url = environment.API_URL+"/api/employee/is-main/"+user.id;
    return this.http.get<Boolean>(url)
  }

  setMainBartender(user: UserDetalis, main: boolean):Observable<Boolean>
  { 
    var url = environment.API_URL+"/api/employee/set-main-bartender/"+user.id+"?mainBartender="+main.toString();
    return this.http.put<Boolean>(url, {})
  }

  setMainCook(user: UserDetalis, main: boolean):Observable<Boolean>
  { 
    var url = environment.API_URL+"/api/employee/set-main-cook/"+user.id+"?mainCook="+main.toString();
    return this.http.put<Boolean>(url, {})
  }


  //requests vraca hranu ali se ne koristi nigde...

  acceptFoodRequest(requestId:Number):Observable<any>
  {
    var url = environment.API_URL+"/api/food-request/accept-food/"+requestId;
    return this.http.put<any>(url, {})
  }

  declineFoodRequest(requestId:Number):Observable<any>
  {
    var url = environment.API_URL+"/api/food-request/decline-food/"+requestId;
    return this.http.put<any>(url, {})
  }

  acceptDrinkRequest(requestId:Number):Observable<any>
  {
    var url = environment.API_URL+"/api/drink-request/accept-drink/"+requestId;
    return this.http.put<any>(url, {})
  }

  declineDrinkRequest(requestId:Number):Observable<any>
  {
    var url = environment.API_URL+"/api/drink-request/decline-drink/"+requestId;
    return this.http.put<any>(url, {})
  }

  //reports

  getTotalTurnoverThisMonthReport():Observable<TurnoverList>
  {
    var url = environment.API_URL+"/api/reports/turnover-this-month";
    return this.http.get<any>(url)
  }

  getTotalTurnoverThisYearReport():Observable<TurnoverList>
  {
    var url = environment.API_URL+"/api/reports/turnover-this-year";
    return this.http.get<any>(url)
  }

  getEmployeeNumberReport():Observable<ChartJs>
  {
    var url = environment.API_URL+"/api/reports/employee-number";
    return this.http.get<ChartJs>(url)
  }

  getEmployeeTotalSalaryReport():Observable<ChartJs>
  {
    var url = environment.API_URL+"/api/reports/employee-salaries";
    return this.http.get<ChartJs>(url)
  }

  getDailyTargetInfo():Observable<TurnoverList>
  {
    var url = environment.API_URL+"/api/reports/target-income-report";
    return this.http.get<any>(url)
  }

  getProfitsDaily():Observable<TurnoverList>
  {
    var url = environment.API_URL+"/api/reports/total-income-daily";
    return this.http.get<any>(url)
  }

  getProfitsMonthly():Observable<TurnoverList>
  {
    var url = environment.API_URL+"/api/reports/total-income-monthly";
    return this.http.get<any>(url)
  }

  getFoodRequest(id:Number):Observable<Request[]>
  {
    var url = environment.API_URL+"/api/food-request/"+id;
    return this.http.get<Request[]>(url) 
   }

   getDrinkRequest(id:Number):Observable<Request[]>
   {
     var url = environment.API_URL+"/api/drink-request/"+id;
     return this.http.get<Request[]>(url) 
    }

  topArticlesReport():Observable<TopArticle[]>
  {
    var url = environment.API_URL+"/api/reports/top-articles";
    return this.http.get<TopArticle[]>(url)
  }

}
