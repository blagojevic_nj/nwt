import { Routes } from "@angular/router";
import { ActivationGuard } from "./guards/activation/activation.guard";
import { LoginGuard } from "./guards/login/login.guard";
import { ActivateComponent } from "./pages/activate/activate.component";
import { LoginComponent } from "./pages/login/login.component";

export const AuthRoutes: Routes = [
  {
    path: "login",
    pathMatch: "full",
    component: LoginComponent,
    canActivate: [LoginGuard],
  },

  {
    path: "activate",
    pathMatch: "full",
    component: ActivateComponent,
    canActivate: [ActivationGuard]
  },
];
