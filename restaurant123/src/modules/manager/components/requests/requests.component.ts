import { DataSource } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import RequestSmall from 'src/modules/shared/models/manager/request-small';
import { ManagerService } from '../../services/manager.service';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.scss']
})
export class RequestsComponent implements OnInit {

  requests: RequestSmall[] = [];
  dataSource = new ExampleDataSource(this.requests);
  numberOfElements: number = 0;
  searchParam: string = "";
  filterParam: string = "";
  currentPage: number = 0;
  requestType: string = 'food';




  constructor(private managerService: ManagerService) {
  }

  ngOnInit(): void {
    this.getAllRequestsForPage();  
}

  changePage(pageNumber : number) {

    this.currentPage = pageNumber - 1;
    this.getAllRequestsForPage();
  }

  getAllRequestsForPage(): void {
    if(this.requestType==='food')
    {
        this.managerService
        .getFoodRequestsList(this.currentPage, this.searchParam)
        .subscribe((res) => {
          if(res)
            this.requests = [];
            this.numberOfElements = res.totalElements;
            this.requests = res.objects;
            this.requests = this.requests.map(r =>
              {
                r.picture = 'http://localhost:8080' +r.picture;
                return r;
              })
            this.dataSource.setData(this.requests);
        });
    }
    else{
        this.managerService
        .getDrinkRequestsList(this.currentPage, this.searchParam)
        .subscribe((res) => {
          if(res)
            this.requests = [];
            this.numberOfElements = res.totalElements;
            this.requests = res.objects;
            this.requests = this.requests.map(r =>
              {
                r.picture = 'http://localhost:8080' +r.picture;
                return r;
              })
            this.dataSource.setData(this.requests);
        });
    }

  }


  setFood()
  {
      this.requestType = 'food'
      this.getAllRequestsForPage()
  }

  setDrink()
  {
      this.requestType = 'drink'
      this.getAllRequestsForPage()
  }
}


class ExampleDataSource extends DataSource<RequestSmall> {
    private _dataStream = new ReplaySubject<RequestSmall[]>();
  
    constructor(initialData: RequestSmall[]) {
      super();
      this.setData(initialData);
    }
  
    connect(): Observable<RequestSmall[]> {
      return this._dataStream;
    }
  
    disconnect() {}
  
    setData(data: RequestSmall[]) {
      this._dataStream.next(data);
    }
  }