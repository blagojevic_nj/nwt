import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from '@angular/router';
import { NotFoundPageComponent } from './pages/not-found-page/not-found-page.component';
import { RootLayoutComponent } from './pages/root-layout/root-layout.component';
import { EmployeePanelsComponent } from './components/employee-panels/employee-panels.component';

const routes: Routes = [
  {
    path: "restaurant",
    component: RootLayoutComponent,
    children: [
      {
        path: "auth",
        loadChildren: () =>
          import("./../auth/auth.module").then((m) => m.AuthModule),
      },
      {
        path: "users",
        loadChildren: () =>
          import("./../users/users.module").then((m) => m.UsersModule),
      },
      {
        path: "drinks",
        loadChildren: () =>
          import("./../drinks/drinks.module").then((m) => m.DrinksModule),
      },
      {
        path: "food",
        loadChildren: () =>
          import("./../food/food.module").then((m) => m.FoodModule),
      },
      {
        path: "orders",
        loadChildren: () =>
          import("./../orders/orders.module").then((m) => m.OrdersModule),
      },
      {
        path: "panels",
        component: EmployeePanelsComponent,
      },
      {
        path: "manager",
        loadChildren: () =>
          import("./../manager/manager.module").then((m) => m.ManagerModule),
      },
      {
        path: "owner",
        loadChildren: () =>
          import("./../owner/owner.module").then((m) => m.OwnerModule),
      },
      {
        path: "floor-plan",
        loadChildren: () =>
          import("./../floor-plan/floor-plan.module").then((m) => m.FloorPlanModule),
      },
    ],
  },
  {
    path: "",
    redirectTo: "restaurant/auth/activate",
    pathMatch: "full"
  },
  {
    path: "**", component: NotFoundPageComponent
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
