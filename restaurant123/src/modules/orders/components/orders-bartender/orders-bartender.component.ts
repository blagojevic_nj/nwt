import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import ActiveOrder from 'src/modules/shared/models/orders/active-order';
import { OrderService } from '../../service/order.service';
import OrderArticleStatusUpdate from 'src/modules/shared/models/order-articles/article-status-update';
import ArticlesStatus from 'src/modules/shared/models/order-articles/articles-status';

import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-orders-bartender',
  templateUrl: './orders-bartender.component.html',
  styleUrls: ['./orders-bartender.component.scss']
})
export class OrdersBartenderComponent implements OnInit {

  inProgress: any[] = [];
  done: any[] = [];
  todo: any[] = [];

  orders: ActiveOrder[] = [];

  timeoutHandler: any;
  private serverUrl = environment.API_URL + '/socket'
  private stompClient: any;

  isLoaded: boolean = false;
  isCustomSocketOpened = false;

  constructor(private orderService: OrderService) { }

  ngOnInit(): void {

    this.getAllOrders();
    this.initializeWebSocketConnection();
  }

  public mouseup() {
    if (this.timeoutHandler) {
      clearTimeout(this.timeoutHandler);
      this.timeoutHandler = null;
    }
  }

  public mousedown() {
    this.timeoutHandler = setTimeout(() => {
      this.timeoutHandler = null;
      this.mouseup();
    }, 1000);
  }

  drop(event: CdkDragDrop<any[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex,
      );
    }
  }

  dropGroup(event: CdkDragDrop<any[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      if(this.changeStatus(event.previousContainer.data[event.previousIndex], event.container.id)){
        transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex,
      );
      this.refactor(event.container.data);
     }
    }
  }

  changeStatus(obj: any, status: string): boolean{
    if(this.isGroup(obj)){
      const order: ActiveOrder = obj;
      let ids: number[] = [];
      order.orderArticles.forEach((art)=> ids.push(art.id));
      const newStatus: ArticlesStatus = {
        articles: ids,
        status: status
      }
      if(order.orderArticles.length != 0){
        this.orderService.changeArticlesStatus(newStatus).subscribe(
        (result) => {
          order.orderArticles.forEach((art)=> art.orderArticleStatus = status);
          return result;
        }, 
        (error) => {
          return false;
        }
      );
      }
      
    } else{
      const newStatus: OrderArticleStatusUpdate = {
        newStatus: status
      } 
      this.orderService.changeStatus(newStatus, obj.id).subscribe(
        (result) => {
          obj.orderArticleStatus = status;
          return result;
        }, 
        (error) => {
          return false;
        }
      );
    }

    return true;
  }

  getAllOrders(): void{
    this.orderService.getAll().subscribe(
      (result) => {
        this.orders = [];
        this.orders = result;
        this.todo = []
        this.inProgress = []
        this.done = []
        this.rescheduleOrders();
      },
      (error) => {

      }
    );
  }

  rescheduleOrders():void {
    this.orders.forEach((order) => {
      let status = "PENDING";
      order.orderArticles.forEach((article) => {
        article.parent = order.id;
        if(article.orderArticleStatus == "PENDING")
          this.todo.push(article);
        else if(article.orderArticleStatus == "IN_PROGRESS"){
          this.inProgress.push(article);
          status = "IN_PROGRESS";
        }
        else if(article.orderArticleStatus == "READY")
        {
          this.done.push(article);
          status = "READY";
        }
      })
      order.orderArticles = [];
      if(status == "PENDING"){
        this.todo.push(order);
        this.refactor(this.todo);
      }
      else if(status == "IN_PROGRESS"){
        this.inProgress.push(order);
        this.refactor(this.inProgress);
      }
      else{ 
        this.done.push(order);
        this.refactor(this.done);
      }
    })
  }

  isGroup(obj: Object): boolean {
    return obj.hasOwnProperty('orderArticles');
  }

  private refactor(array: any[]): void{
    const forSplice: any[] = [];
    array.forEach((elem) => {
      if(!this.isGroup(elem)){
        const group = array.filter((i) => (this.isGroup(i) && i.id === elem.parent));
        if (group.length === 1){
          group[0].orderArticles.push(elem);
          forSplice.push(elem);
        }
      }
    })

    forSplice.forEach((elem) => {
      const index = array.indexOf(elem);
      array.splice(index, 1);})
  }

  toTime (date:number):string {
    return (new Date(date)).toString().split(' ')[4]
  }

    // Funkcija za otvaranje konekcije sa serverom
    initializeWebSocketConnection() {
      // serverUrl je vrednost koju smo definisali u registerStompEndpoints() metodi na serveru
      let ws = new SockJS(this.serverUrl);
      this.stompClient = Stomp.over(ws);
      let that = this;
  
      this.stompClient.connect({}, function () {
        that.isLoaded = true;
        that.openGlobalSocket()
      });
    }
  
    // Funckija za pretplatu na topic /socket-publisher (definise se u configureMessageBroker() metodi)
    // Globalni socket se otvara prilikom inicijalizacije klijentske aplikacije
    openGlobalSocket() {
      if (this.isLoaded) {
        this.stompClient.subscribe('/order-created', (response: any) => {
          this.getAllOrders();
        });

        this.stompClient.subscribe('/order-cancelled', (response: any) => {
          this.getAllOrders();
        });

        this.stompClient.subscribe('/order-finished', (response: any) => {
          this.getAllOrders();
        });

        if (sessionStorage.getItem("userRole") == "COOK") {
          this.stompClient.subscribe('/order-updated-cook', (response: any) => {
            this.getAllOrders();
          });
        } else {
          this.stompClient.subscribe('/order-updated-bartender', (response: any) => {
            this.getAllOrders();
          });
        }
      }
    }
  
    // Funkcija za pretplatu na topic /socket-publisher/user-id
    // CustomSocket se otvara kada korisnik unese svoj ID u polje 'fromId' u submit callback-u forme 'userForm'
    openSocket() {
      if (this.isLoaded) {
        this.isCustomSocketOpened = true;
        //this.stompClient.subscribe("/socket-publisher/" + this.userForm.value.fromId, (message: { body: string; }) => {
          //this.handleResult(message);
        //});
      }
    }
  
    // Funkcija koja se poziva kada server posalje poruku na topic na koji se klijent pretplatio
    handleResult(message: { body: string; }) {
      
    }

}
