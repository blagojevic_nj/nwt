import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/modules/auth/services/auth-service/auth.service';
import { SharedService } from 'src/modules/root/services/shared.service';

@Component({
  selector: 'app-root-layout',
  templateUrl: './root-layout.component.html',
  styleUrls: ['./root-layout.component.scss']
})
export class RootLayoutComponent implements OnInit {

  constructor(public sharedService: SharedService, public authService: AuthService) {
   }

  ngOnInit(): void {
  }

  showHeader(): boolean {
    return sessionStorage.getItem('user') !== null || sessionStorage.getItem('employee') !== null;
  }
}
