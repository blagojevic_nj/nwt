import { Component, Input, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { CheckoutArticle } from 'src/modules/shared/models/checkout/CheckoutArticle';

@Component({
  selector: 'app-checkout-list-item',
  templateUrl: './checkout-list-item.component.html',
  styleUrls: ['./checkout-list-item.component.scss']
})
export class CheckoutListItemComponent implements OnInit {

  @Input() checkoutItem?: CheckoutArticle;
  img = "";
  constructor() { }

  ngOnInit(): void {
    this.img = `${environment.API_URL}${this.checkoutItem?.image}`; 
    console.log(this.img);
  }

}
