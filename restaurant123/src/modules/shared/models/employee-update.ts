export default interface EmployeeUpdate{
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    contactPhone: string;
}