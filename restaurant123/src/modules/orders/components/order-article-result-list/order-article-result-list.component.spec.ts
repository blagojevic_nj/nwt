import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderArticleResultListComponent } from './order-article-result-list.component';

describe('OrderArticleResultListComponent', () => {
  let component: OrderArticleResultListComponent;
  let fixture: ComponentFixture<OrderArticleResultListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderArticleResultListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderArticleResultListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
