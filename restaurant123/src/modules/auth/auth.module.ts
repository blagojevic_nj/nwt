import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { LoginComponent } from "./pages/login/login.component";
import { ReactiveFormsModule } from "@angular/forms";

import { AuthRoutes } from "./auth.routes";
import { ActivateComponent } from './pages/activate/activate.component';
import { HttpClientModule } from '@angular/common/http';

import { MaterialModule } from '../shared/material.module';

@NgModule({
  declarations: [LoginComponent, ActivateComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(AuthRoutes),
    HttpClientModule,
    MaterialModule,
  ],
  providers: [],
})
export class AuthModule { }
