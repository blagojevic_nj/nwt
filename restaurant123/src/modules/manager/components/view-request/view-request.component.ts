import { Component, Input, OnInit,ChangeDetectorRef, AfterViewChecked } from '@angular/core';
import { coordsToEvent } from '@interactjs/utils/pointerUtils';
import Request from 'src/modules/shared/models/request';

@Component({
  selector: 'app-view-request',
  templateUrl: './view-request.component.html',
  styleUrls: ['./view-request.component.scss']
})
export class ViewRequestComponent implements OnInit, AfterViewChecked {

  @Input() data: Request|undefined
  type:             String | undefined = ""
  name:             string | undefined = ""
  preparationPrice: number | undefined = 1
  preparationTime:  number | undefined = 1
  picture:          string | undefined = ""
  description:      string | undefined = ""
  price:            Number | undefined = 1
  group:            String | undefined = ""
  CookName:         String | undefined = ""
  requestType:      String | undefined = ""
  
  constructor(private cdr: ChangeDetectorRef) {}

  ngAfterViewChecked(): void {
    this.cdr.detectChanges();
  }

  ngOnInit(): void {
    if(this.data)
    {
      console.trace(this.data)
      this.type = this.data?.type
      this.name = this.data?.name
      this.preparationPrice = this.data?.preparationPrice
      this.preparationTime = this.data?.preparationTime
      this.picture = "http://localhost:8080"+this.data?.picture
      this.description = this.data?.description
      this.price =this.data?.price
      this.group = this.data?.group
      this.CookName = this.data?.cookName,
      this.data.requestType === "UPDATE" ? this.requestType="Izmenu" :
      this.data.requestType === "DELETE" ?  this.requestType="Brisanje":
      this.data.requestType === "CREATE" ? this.requestType="Dodavanje":{};
        

    }
  }

}
