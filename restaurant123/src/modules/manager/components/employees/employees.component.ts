import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Observable, ReplaySubject } from 'rxjs';
import { ManagerService } from '../../services/manager.service';
import Employee from 'src/modules/shared/models/employees/employee';
import { Router, RouterModule } from '@angular/router';


@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {

    employees: Employee[] = [];
    dataSource = new ExampleDataSource(this.employees);
    numberOfElements: number = 0;
    searchParam: string = "";
    currentPage: number = 0;
    requestType: string = 'allEmployees';

  constructor(private managerService:ManagerService, private router:Router) { }


  ngOnInit(): void {
    this.getAllEmployeesForPage();  
}

  changePage(pageNumber : number) {

    this.currentPage = pageNumber - 1;
    this.getAllEmployeesForPage();
  }

  getAllEmployeesForPage(): void {
    if(this.requestType==='allEmployees')
    {
        this.managerService
        .getAllEmployedList(this.currentPage, this.searchParam)
        .subscribe((res) => {
          if(res)
            this.employees = [];
            this.numberOfElements = res.totalElements;
            this.employees = res.objects.filter(e =>e.salary!=="0.0");
            this.dataSource.setData(this.employees);
        });
    }
    else if(this.requestType==='cooks'){
        this.managerService
        .getAllCooksList(this.currentPage, this.searchParam)
        .subscribe((res) => {
          if(res)
          this.employees = [];
          this.numberOfElements = res.totalElements;
          this.employees = res.objects.filter(e =>e.salary!=="0.0");
          this.dataSource.setData(this.employees);
        });
    }
    else if(this.requestType==='waiters'){
        this.managerService
        .getAllWaitersList(this.currentPage, this.searchParam)
        .subscribe((res) => {
          if(res)
          this.employees = [];
          this.numberOfElements = res.totalElements;
          this.employees = res.objects.filter(e =>e.salary!=="0.0");
          this.dataSource.setData(this.employees);
        });
    }
    else if(this.requestType==='barteners'){
        this.managerService
        .getAllBartendersList(this.currentPage, this.searchParam)
        .subscribe((res) => {
          if(res)
          this.employees = [];
          this.numberOfElements = res.totalElements;
          this.employees = res.objects.filter(e =>e.salary!=="0.0");
          this.dataSource.setData(this.employees);
        });
    }
    else if(this.requestType==='notEmployed'){
        this.managerService
        .getAllNotEmployedList(this.currentPage, this.searchParam)
        .subscribe((res) => {
          if(res)
          this.employees = [];
          this.numberOfElements = res.totalElements;
          this.employees = res.objects.filter(e =>e.salary==="0.0");
          this.dataSource.setData(this.employees);
        });
    }

  }


  setAll()
  {
      this.requestType = 'allEmployees'
      this.getAllEmployeesForPage()
  }

  setCooks()
  {
      this.requestType = 'cooks'
      this.getAllEmployeesForPage()
  }

  setBartenders()
  {
      this.requestType = 'barteners'
      this.getAllEmployeesForPage()
  }

  setWaiters()
  {
      this.requestType = 'waiters'
      this.getAllEmployeesForPage()
  }

  setNotEmployed()
  {
      this.requestType = 'notEmployed'
      this.getAllEmployeesForPage()
  }

  search(searchedText : any): void {

    if (searchedText != null) {
      
      this.searchParam = searchedText.target.value;
      this.getAllEmployeesForPage();
    }
  }

  viewUserDetalies(id: number):void {
    this.router.navigate(['/restaurant/manager/user/' +id])
  }

}


class ExampleDataSource extends DataSource<Employee> {
    private _dataStream = new ReplaySubject<Employee[]>();
  
    constructor(initialData: Employee[]) {
      super();
      this.setData(initialData);
    }
  
    connect(): Observable<Employee[]> {
      return this._dataStream;
    }
  
    disconnect() {}
  
    setData(data: Employee[]) {
      this._dataStream.next(data);
    }
  }