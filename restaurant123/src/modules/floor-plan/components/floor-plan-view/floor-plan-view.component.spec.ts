import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FloorPlanViewComponent } from './floor-plan-view.component';

describe('FloorPlanViewComponent', () => {
  let component: FloorPlanViewComponent;
  let fixture: ComponentFixture<FloorPlanViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FloorPlanViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FloorPlanViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
