import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Checkout } from 'src/modules/shared/models/checkout/Checkout';
import { OrderService } from '../../service/order.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {

  orderId: number = -1; 
  checkout?: Checkout;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private toastr: ToastrService,
    private orderService: OrderService,
    public matDialogRef: MatDialogRef<CheckoutComponent>) { }

  ngOnInit(): void {
    this.orderService.getLastOrderForTable(this.data.tableId).subscribe((res) => {
      this.orderService.getCheckout(res.id).subscribe(
        (res) => {
          this.checkout = res;
          this.orderId = res.id;
        },
        () => {
          this.toastr.error("Greška prilikom dobavljanja računa. Proverite da li su sve stavke porudžbine dostavljene.");
          this.matDialogRef.close("fail");
        }
      )
    })
    
  }

  submit() {
    this.orderService.finishOrder(this.orderId).subscribe(
      () => {
        this.toastr.success("Uspoešno izvršeno");
        this.matDialogRef.close("success");
      },
      () => {this.toastr.error("Greška prilikom dobavljanja računa. Proverite da li su sve stavke porudžbine dostavljene.");}
      )
  }

}
