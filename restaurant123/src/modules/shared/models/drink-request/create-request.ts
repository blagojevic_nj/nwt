import IngredientInfo from "../ingredient-info";

export default interface DrinkCreate{
    name: string,
    picture: string,
    description: string,
    preparationPrice: number,
    ingredients: IngredientInfo[],
    alcohol: boolean
}