import { Routes } from "@angular/router";
import { EmployeeGuard } from "../auth/guards/employee/employee.guard";
import { FoodCreateFormComponent } from "./components/food-create-form/food-create-form.component";
import { FoodInfoComponent } from "./components/food-info/food-info.component";
import { FoodListComponent } from "./components/food-list/food-list.component";
import { FoodUpdateFormComponent } from "./components/food-update-form/food-update-form.component";

export const FoodRoutes: Routes = [
    {
      path: "",
      pathMatch: "full",
      component: FoodListComponent,
      canActivate: [EmployeeGuard],
      data: {expectedTypes: "COOK"}
    },
    {
      path: ":id",
      pathMatch: "full",
      component: FoodInfoComponent,
      canActivate: [EmployeeGuard],
      data: {expectedTypes: "COOK"}
    },
    {
      path: "form/create",  //promeni samo na form (i u dugmetu za redirekciju, i u e2e testovima)
      pathMatch: "full",
      component: FoodCreateFormComponent,
      canActivate: [EmployeeGuard],
      data: {expectedTypes: "COOK"}
    },
    {
      path: "form/:id",
      pathMatch: "full",
      component: FoodUpdateFormComponent,
      canActivate: [EmployeeGuard],
      data: {expectedTypes: "COOK"}
    },
  ];