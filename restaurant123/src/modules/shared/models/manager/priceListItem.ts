export default interface PricelistItem{
    id:number,
    name: string,
    picture: string,
    description: string,
    price: number,
    preparationPrice: number

}