import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersListComponent } from './components/users-list/users-list.component';
import { RouterModule } from "@angular/router";
import { UsersRoutes } from './users.routes';
import { MaterialModule } from '../shared/material.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserAddDialogComponent } from './components/user-add-dialog/user-add-dialog.component';
import { UserUpdateDialogComponent } from './components/user-update-dialog/user-update-dialog.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChangePasswordDialogComponent } from './components/change-password-dialog/change-password-dialog.component';
import { MyProfileComponent } from './components/my-profile/my-profile.component';
@NgModule({
  declarations: [
    UsersListComponent,
    UserAddDialogComponent,
    UserUpdateDialogComponent,
    ChangePasswordDialogComponent,
    MyProfileComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(UsersRoutes),
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ]
})
export class UsersModule { }
