import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from '../../services/shared.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  constructor(public sharedService: SharedService, private router:Router, private toast: ToastrService) { }

  ngOnInit(): void {
  }

  toggleIzvestaj(event:any):void {
    
    event.stopPropagation();
    var nav = document.getElementById('reportsNav') || {style:{display:{}}};
    console.trace(nav)
    if (nav && nav.style.display === "none"|| nav.style.display==="") {
      nav.style.display = "block";
    } else {
      nav.style.display = "none";
    }
  }
  logout(): void{
    sessionStorage.removeItem("user");
    this.router.navigate(["/restaurant/auth/login"])
  }

  deactivate(): void {
    sessionStorage.removeItem("activated");
    sessionStorage.removeItem("employee");
    sessionStorage.removeItem("pin");
    sessionStorage.removeItem("userRole");
    this.toast.success("Aplikacija deaktivirana!")
  }

  employeeLogout(): void {
    sessionStorage.removeItem('employee');
    sessionStorage.removeItem('pin');
    sessionStorage.removeItem("userRole");
    this.toast.success("Uspesna odjava!")
    this.router.navigate(["/restaurant/panels"])
  }
}
