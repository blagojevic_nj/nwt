import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable()
export class Interceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const item = sessionStorage.getItem("user");

    if (item) {
      const cloned = req.clone({
        headers: req.headers.set("Authorization", "Bearer "+JSON.parse(item)),
      });

      return next.handle(cloned);
    } else {
      return next.handle(req);
    }
  }
}
