import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Output } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import EmployeeBasic from 'src/modules/shared/models/employees/employee-basic';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  
  private headers = new HttpHeaders({ "Content-Type": "application/json" });
  navSidebarClass: boolean = true;
  hamburgerClass: boolean = false;

  constructor(private http: HttpClient) { }
  
  toggleSidebarClass() {
	return this.navSidebarClass = !this.navSidebarClass  ;
  }
  toggleHamburgerClass() {
	return this.hamburgerClass = !this.hamburgerClass  ;
  }
    

  checkRole(): string {
    const item = sessionStorage.getItem("user");

    if (item) {
      const jwt: JwtHelperService = new JwtHelperService();
      return jwt.decodeToken(item).role;
    }
    return "";
  }

  checkType(): string {
    const item = sessionStorage.getItem("employee");

    if (item) {
      const employee = JSON.parse(item);
      return employee.employeeType;
    }
    return "";
  }
  
  isActivated(): boolean {
    const item = sessionStorage.getItem("activated");
    if(!item)
      return false;

    return JSON.parse(item);
  }

  getByPin(pin: string): Observable<EmployeeBasic> {
    return this.http.get<EmployeeBasic>(environment.API_URL+"/api/employee?pin="+pin);
  }
}
