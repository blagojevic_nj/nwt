import IngredientInfo from "../ingredient-info";
import FoodCategory from "./food-category";
import FoodGroup from "./food-group";

export default interface FoodCreateAndUpdate{
    name: string,
    picture: string,
    description: string,
    preparationPrice: number,
    preparationTime: number,
    ingredientIds: number[],
    foodGroup: FoodGroup,
    foodCategoryId: number
}