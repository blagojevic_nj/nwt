export interface UserAll {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    salary: string;
    userType: string;
}