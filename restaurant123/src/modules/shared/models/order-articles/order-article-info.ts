export interface OrderArticleInfo {
    id?: number,
    pricelistItemId?: number,
    name: string,
    quantity: number,
    price: number,
    status: string
}