type OrderArticleStatus = "PENDING" | "IN_PROGRESS" | "READY" | "DELIVERED" | "CANCELLED";

export interface OrderArticleCreate {
    id?: number,
    pricelistItemId: number,
    quantity: number,
    status?: OrderArticleStatus
}