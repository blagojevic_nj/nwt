export default interface ElementForSelection {
    id: number;
    name: string;
}