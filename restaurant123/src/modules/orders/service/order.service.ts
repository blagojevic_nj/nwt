import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Allergen } from 'src/modules/shared/models/allergen/allergen';
import { Checkout } from 'src/modules/shared/models/checkout/Checkout';
import { MenuItemType } from 'src/modules/shared/models/menu-item-type/menu-item-type';
import OrderArticleStatusUpdate from 'src/modules/shared/models/order-articles/article-status-update';
import ArticlesStatus from 'src/modules/shared/models/order-articles/articles-status';
import { OrderArticleCreate } from 'src/modules/shared/models/order-articles/order-article-create';
import { OrderArticleInfo } from 'src/modules/shared/models/order-articles/order-article-info';
import ActiveOrder from 'src/modules/shared/models/orders/active-order';
import { CreateOrder } from 'src/modules/shared/models/orders/create-order';
import OrderInfo from 'src/modules/shared/models/orders/order-info';
import PricelistItemSearch from 'src/modules/shared/models/pricelist-item/pricelist-item';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  constructor(private http: HttpClient) { }

  getAll(): Observable<ActiveOrder[]> {
    const headers = new HttpHeaders({ "Content-Type": "application/json", "employee-pin": this.getPin()});
    if (sessionStorage.getItem("userRole") == "COOK") {
      return this.http.get<ActiveOrder[]>(environment.API_URL+"/api/orders/active/food", {headers: headers});
    } else {
      return this.http.get<ActiveOrder[]>(environment.API_URL+"/api/orders/active/drink", {headers: headers});
    }
  }

  changeArticlesStatus(articlesStatus: ArticlesStatus): Observable<boolean> {
    const headers = new HttpHeaders({"employee-pin": this.getPin()});
    return this.http.patch<boolean>(environment.API_URL+"/api/order-articles/change-status", articlesStatus, {headers: headers});
  }

  changeStatus(articleStatus: OrderArticleStatusUpdate, id: number): Observable<boolean> {
    const headers = new HttpHeaders({"employee-pin": this.getPin()});
    if (sessionStorage.getItem("userRole") == "COOK") {
      return this.http.post<boolean>(environment.API_URL+"/api/order-articles/status/" + id, articleStatus, {headers: headers});
    } else {
      return this.http.post<boolean>(environment.API_URL+"/api/order-articles/change-status/" + id, articleStatus, {headers: headers});
    }
  }

  searchOrders(allergens: Allergen[], search: string, type: MenuItemType): Observable<PricelistItemSearch[]> {
    const headers = new HttpHeaders({"employee-pin": this.getPin()});
    let url = `${environment.API_URL}/api/pricelist/search?searchParam=${search}&type=${type.toString()}`;
    for (const a of allergens) {
      url += `&allergen=${a.toString()}`;
    }
    console.log(url)
    return this.http.get<PricelistItemSearch[]>(url, {headers: headers});
  }

  addOrder(data: CreateOrder): Observable<any> {
    let url = `${environment.API_URL}/api/orders`;
    console.log(data)
    const headers = new HttpHeaders({"employee-pin": this.getPin()});
    return this.http.post(url, data, {headers: headers})
  }

  getLastOrderForTable(tableId: number): Observable<OrderInfo> {
    const headers = new HttpHeaders({"employee-pin": this.getPin()});
    let url = `${environment.API_URL}/api/orders/table/${tableId}`;
    return this.http.get<OrderInfo>(url, {headers: headers});
  }

  getPricelistItem(id: number): Observable<PricelistItemSearch> {
    const headers = new HttpHeaders({"employee-pin": this.getPin()});
    let url = `${environment.API_URL}/api/pricelist/pricelist-item/${id}`;
    return this.http.get<PricelistItemSearch>(url, {headers: headers});
  }

  updateNote(note: string, id: number): Observable<PricelistItemSearch> {
    const headers = new HttpHeaders({"employee-pin": this.getPin()});
    let url = `${environment.API_URL}/api/orders/note/${id}`;
    return this.http.put<any>(url, {note: note}, {headers: headers});
  }

  insertArticle(article: OrderArticleCreate, orderId: number): Observable<PricelistItemSearch> {
    const headers = new HttpHeaders({"employee-pin": this.getPin()});
    let url = `${environment.API_URL}/api/order-articles`;
    return this.http.post<any>(url, {orderId: orderId, pricelistItemId: article.pricelistItemId, quantity: article.quantity}, {headers: headers});
  }

  removeArticle(id: number) {
    const headers = new HttpHeaders({"employee-pin": this.getPin()});
    let url = `${environment.API_URL}/api/order-articles/cancel/${id}`;
    return this.http.post<any>(url, {}, {headers: headers});
  }

  getCheckout(id: number): Observable<Checkout> {
    const headers = new HttpHeaders({"employee-pin": this.getPin()});
    let url = `${environment.API_URL}/api/orders/checkout/${id}`;
    return this.http.get<Checkout>(url, {headers: headers});
  }

  finishOrder(id: number) {
    const headers = new HttpHeaders({"employee-pin": this.getPin()});
    let url = `${environment.API_URL}/api/orders/${id}/finish`;
    return this.http.put<Checkout>(url, {}, {headers: headers});
  }

  deliverArticle(id: number) {
    const headers = new HttpHeaders({"employee-pin": this.getPin()});
    let url = `${environment.API_URL}/api/order-articles/${id}/deliver`;
    return this.http.put(url, {}, {headers: headers, responseType: 'text'});
  }

  getPin():string {
    const pin = sessionStorage.getItem("pin");
    if(pin) return pin;
    return '';
  }
}
