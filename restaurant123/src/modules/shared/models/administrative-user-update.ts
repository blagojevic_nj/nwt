export default interface AdministrativeUserUpdate{
    id: number;
    firstName: string;
    lastName: string;
    email: string;
}