import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import ActivationRequest from 'src/modules/shared/models/activation/activation-request';
import { AuthService } from '../../services/auth-service/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-activate',
  templateUrl: './activate.component.html',
  styleUrls: ['./activate.component.scss']
})
export class ActivateComponent implements OnInit {

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private toastr: ToastrService,
    private router: Router
  ) {
    this.form = this.fb.group({
      activationPassword: [null, Validators.required],
    });
  }

  ngOnInit(): void {
  }

  submit() {
    if(this.form.valid){
      const request: ActivationRequest = {
        activationPassword: this.form.controls['activationPassword'].value
      }

      this.authService.activate(request).subscribe(
        (result) => {
          this.toastr.success("Uspešno aktivirana aplikacija!");
          sessionStorage.setItem("activated", "true");
          this.router.navigate(["/restaurant/panels"])
          
        },
        (error) => {
          this.toastr.error("Uneta šifra nije validna!");
          sessionStorage.setItem("activated", "false");
        }
      );
    }
  }
}
