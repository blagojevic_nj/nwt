import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { CheckoutComponent } from 'src/modules/orders/components/checkout/checkout.component';
import { CreateOrderModalComponent } from 'src/modules/orders/components/create-order-modal/create-order-modal.component';
import { OrderService } from 'src/modules/orders/service/order.service';
import { PositionedTableElement } from 'src/modules/shared/models/floor-plan/positioned-table-element';
import { FloorPlanService } from '../../services/floor-plan.service';

@Component({
  selector: 'app-table-options',
  templateUrl: './table-options.component.html',
  styleUrls: ['./table-options.component.scss']
})
export class TableOptionsComponent implements OnInit {

  @Input() table?: PositionedTableElement;
  @Output() onTableUpdated: EventEmitter<any> = new EventEmitter();

  constructor(public dialog: MatDialog, private floorPlanService: FloorPlanService, private orderService: OrderService, private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  showOrderModal() {
    this.table!.notification = false;
    const dialogRef = this.dialog.open(CreateOrderModalComponent, {
      data: { tableId: this.table?.id || -1, mode: "EDIT" },
      width: '80%',
      height: '100%',
    });
  }

  occupyTable() {
    const dialogRef = this.dialog.open(CreateOrderModalComponent, {
      data: { tableId: this.table?.id || -1, mode: "CREATE" },
      width: '80%',
      height: '100%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'success') {
        this.onTableUpdated.emit();
      }
    });
  }

  checkout() {
    const dialogRef = this.dialog.open(CheckoutComponent, {data: { tableId: this.table?.id || -1 }});
      
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'success') {
        this.onTableUpdated.emit();
      }
    });
  }

  free() {
    this.floorPlanService.freeTable(Number(this.table?.id) || -1).subscribe(
      () => {
        this.toastr.success("Uspešno oslobođeno."); 
        this.onTableUpdated.emit();
      },
      () => {this.toastr.error("Došlo je do greške. Pokušajte kasnije.")}
    )
  }

  cancel() {
    this.floorPlanService.cancelTable(Number(this.table?.id) || -1).subscribe(
      () => {
        this.toastr.success("Uspešno otkazano."); 
        this.onTableUpdated.emit();
      },
      () => {this.toastr.error("Došlo je do greške. Pokušajte kasnije.")}
    )
  }
}
