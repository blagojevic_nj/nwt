export default interface AdministrativeUserAdd {
    firstName: string;
    lastName: string;
    email: string;
    username: string;
    password: string;
    administrativeUserType: string;
}