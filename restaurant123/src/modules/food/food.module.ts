import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../shared/material.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FoodListComponent } from './components/food-list/food-list.component';
import { FoodRoutes } from './food.routes';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FoodInfoComponent } from './components/food-info/food-info.component';
import { FoodCreateFormComponent } from './components/food-create-form/food-create-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FoodUpdateFormComponent } from './components/food-update-form/food-update-form.component';4

@NgModule({
  declarations: [
    FoodListComponent,
    FoodInfoComponent,
    FoodCreateFormComponent,
    FoodUpdateFormComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild(FoodRoutes),
    HttpClientModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
  ],
  exports: [
    FoodInfoComponent
  ]
})
export class FoodModule { }