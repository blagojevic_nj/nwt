import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersBartenderComponent } from './components/orders-bartender/orders-bartender.component';
import { MaterialModule } from '../shared/material.module';
import { RouterModule } from '@angular/router';
import { OrdersRoutes } from './orders.routes';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateOrderModalComponent } from './components/create-order-modal/create-order-modal.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { CheckoutListItemComponent } from './components/checkout-list-item/checkout-list-item.component';
import { CheckoutListComponent } from './components/checkout-list/checkout-list.component';
import { OrderSearchComponent } from './components/order-search/order-search.component';
import { OrderArticleListComponent } from './components/order-article-list/order-article-list.component';
import { OrderArticleListItemComponent } from './components/order-article-list-item/order-article-list-item.component';
import { OrderArticleResultListComponent } from './components/order-article-result-list/order-article-result-list.component';
import { OrderArticleResultListItemComponent } from './components/order-article-result-list-item/order-article-result-list-item.component';

@NgModule({
  declarations: [
    OrdersBartenderComponent,
    CreateOrderModalComponent,
    CheckoutComponent,
    CheckoutListItemComponent,
    CheckoutListComponent,
    OrderSearchComponent,
    OrderArticleListComponent,
    OrderArticleListItemComponent,
    OrderArticleResultListComponent,
    OrderArticleResultListItemComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild(OrdersRoutes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class OrdersModule { }
