import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IngredientAddDialogComponent } from './ingredient-add-dialog.component';

describe('IngredientAddDialogComponent', () => {
  let component: IngredientAddDialogComponent;
  let fixture: ComponentFixture<IngredientAddDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IngredientAddDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientAddDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
