import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import NewFoodCategory from 'src/modules/shared/models/food/new-food-category';
import { ToastrService } from 'ngx-toastr';
import { FoodCategoryService } from '../../food-category.service';

@Component({
  selector: 'app-food-category-add-dialog',
  templateUrl: './food-category-add-dialog.component.html',
  styleUrls: ['./food-category-add-dialog.component.scss']
})
export class FoodCategoryAddDialogComponent implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    public matDialogRef: MatDialogRef<FoodCategoryAddDialogComponent>,
    private foodCategoryService: FoodCategoryService,
    private toast: ToastrService
  ) {
    this.form = this.fb.group({
      name: [null, Validators.required],
      description: []
    });
   }

  ngOnInit(): void {
  }
  
  submit(): void{
    if(this.form.value.name){
      const NewFoodCategory: NewFoodCategory = {
      name: this.form.value.name,
      description: this.form.value.description
    }

    this.foodCategoryService.createNewFoodCategory(NewFoodCategory)
    .subscribe((res) => {
      this.toast.success("Uspesno dodata nova kategorija!")
      this.matDialogRef.close();
    })
  } else{
    this.toast.error("Unesite validne podatke!")
  }
}
}




