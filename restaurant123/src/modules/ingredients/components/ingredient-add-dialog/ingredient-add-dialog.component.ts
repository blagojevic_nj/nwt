import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import NewIngredient from 'src/modules/shared/models/ingredients/new-ingredient';
import { IngredientService } from '../../ingredients.service';

@Component({
  selector: 'app-ingredient-add-dialog',
  templateUrl: './ingredient-add-dialog.component.html',
  styleUrls: ['./ingredient-add-dialog.component.scss']
})
export class IngredientAddDialogComponent implements OnInit {
  form: FormGroup;
  allAlergens: string[] = ["MILK", "EGGS", "FISH", "SHELLFISH", "TREE_NUTS", "PEANUTS", "WHEAT", "SOY_BEAN"];

  constructor(
    private fb: FormBuilder,
    public matDialogRef: MatDialogRef<IngredientAddDialogComponent>,
    private ingredientService: IngredientService,
    private toast: ToastrService
  ) {
    this.form = this.fb.group({
      name: [null, Validators.required],
      allergens: []
    });
   }

  ngOnInit(): void {
  }
  
  submit(): void{
    if(this.form.value.name){
      const newIngredient: NewIngredient = {
      name: this.form.value.name,
      allergens: this.form.value.allergens
    }

    this.ingredientService.createNewIngredient(newIngredient)
    .subscribe((res) => {
      this.toast.success("Uspesno dodat sastojak!")
      this.matDialogRef.close();
    })
    } else{
      this.toast.error("Unesite validne podatke!")
    }
  }
}
