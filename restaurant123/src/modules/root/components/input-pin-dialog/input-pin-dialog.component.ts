import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-input-pin-dialog',
  templateUrl: './input-pin-dialog.component.html',
  styleUrls: ['./input-pin-dialog.component.scss']
})
export class InputPinDialogComponent implements OnInit {

  form : FormGroup;
  constructor(
    private fb: FormBuilder,
    public matDialogRef: MatDialogRef<InputPinDialogComponent>,
    private toast: ToastrService
  ) {
    this.form = fb.group({
      pin: [null, Validators.required]
    });
   }

  ngOnInit(): void {
  }

  submit(): void {
    if(this.checkPin()){
      this.matDialogRef.close(this.form.value.pin);
    }
    else{
      this.toast.error("Pin nije validan!");
    }
  }

  checkPin(): boolean {
    if(this.form.value.pin.length != 4) return false;

    try {
      parseInt(this.form.value.pin)
    } catch (error) {
      return false;
    }
    return true;
  }
}
