import IngredientInfo from "../ingredient-info";
import FoodCategory from "./food-category";
import FoodGroup from "./food-group";
import FoodInfoDto from "./food-info-dto";

export default interface FoodInfoInterface {
    id: number;
    name: string,
    picture: string,
    description: string,
    preparationPrice: number,
    preparationTime: number,
    price: number,
    ingredients: IngredientInfo[],
    foodGroup: FoodGroup,
    foodCategory: FoodCategory
}

export default class FoodInfo implements FoodInfoInterface{
    id: number;
    name: string;
    picture: string;
    foodGroup: FoodGroup;
    description: string;
    preparationPrice: number;
    preparationTime: number;
    price: number;
    ingredients: IngredientInfo[];
    foodCategory: FoodCategory;

    constructor(food : FoodInfoDto) {
        this.id = food.id;
        this.name = food.name;
        this.picture = FoodInfo.getPicture(food.picture);
        this.foodGroup = FoodInfo.translateFoodGroupName(food.foodGroup);
        this.description = food.description;
        this.preparationPrice = food.preparationPrice
        this.preparationTime = food.preparationTime
        this.price = food.price
        this.ingredients = food.ingredients
        this.foodCategory = food.foodCategory
    }

    static translateFoodGroupName(group: string) : FoodGroup {

        switch (group) {
            case "MAIN_DISH":
                return {"groupName" : "GLAVNO JELO", "groupEnumValue" : "MAIN_DISH"};
            case "SALAD":
                return {"groupName" : "SALATA", "groupEnumValue" : "SALAD"};
            case "APPETIZER":
                return {"groupName" : "PREDJELO", "groupEnumValue" : "APPETIZER"};
            default:
                return {"groupName" : "DESERT", "groupEnumValue" : "DESERT"};
        }
    }

    static getPicture(picture: string) : string {

        if (picture.length === 0) {
            return "assets/images/food-placeholder.png";
        }
        return picture;
    }
}