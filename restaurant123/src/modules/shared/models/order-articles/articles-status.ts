export default interface ArticlesStatus{
    articles: number[],
    status: string
}