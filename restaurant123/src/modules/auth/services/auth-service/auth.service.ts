import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Login } from 'src/modules/shared/models/login';
import { Token } from 'src/modules/shared/models/token';
import { Observable } from "rxjs";
import { environment } from 'src/environments/environment';
import ActivationRequest from 'src/modules/shared/models/activation/activation-request';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private headers = new HttpHeaders({ "Content-Type": "application/json" });

  constructor(private http: HttpClient) { }

  login(auth: Login): Observable<Token> {
    return this.http.post<Token>(environment.API_URL+"/api/auth/login", auth, {
      headers: this.headers,
      responseType: "json",
    });
  }

  activate(activationRequest: ActivationRequest): Observable<boolean> {
    return this.http.post<boolean>(environment.API_URL+"/api/auth/activate", activationRequest, {
      headers: this.headers,
      responseType: "json",
    });
  }

  isLoggedIn(): boolean {
    if (!sessionStorage.getItem("user")) {
      return false;
    }
    return true;
  }

  getRole(): String {
    const user = sessionStorage.getItem("user");
    if (user !== null){
      return user.toLowerCase();
    }
    return ""
  }
}
