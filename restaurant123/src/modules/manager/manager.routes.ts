import { Routes } from "@angular/router";
import { RoleGuard } from "../auth/guards/role/role.guard";
import { ArticleReportsComponent } from "./components/article-reports/article-reports.component";
import { EmployeeReportsComponent } from "./components/employee-reports/employee-reports.component";
import { EmployeesComponent } from "./components/employees/employees.component";
import { IncomeReportsComponent } from "./components/income-reports/income-reports.component";
import { ManagerHomePageComponent } from "./components/manager-home-page/manager-home-page.component";
import { PricelistComponent } from "./components/pricelist/pricelist.component";
import { RequestDetaliesComponent } from "./components/request-detalies/request-detalies.component";
import { RequestsComponent } from "./components/requests/requests.component";
import { UserProfileComponent } from "./components/user-profile/user-profile.component";


export const ManagerRoutes: Routes = [
  {
    path: "",
    pathMatch: "full",
    component: ManagerHomePageComponent,
    canActivate: [RoleGuard],
    data:{expectedRoles: 'MANAGER|OWNER'}
  },
  {
    path: "employees",
    pathMatch: "full",
    component: EmployeesComponent,
    canActivate: [RoleGuard],
    data:{expectedRoles: 'MANAGER'}
  },
  {
    path: "pricelist",
    pathMatch: "full",
    component: PricelistComponent,
    canActivate: [RoleGuard],
    data:{expectedRoles: 'MANAGER'}
  },
  {
    path: "user/:id",
    pathMatch: "full",
    component: UserProfileComponent,
    canActivate: [RoleGuard],
    data:{expectedRoles: 'MANAGER'}
  },
  {
    path: "requests",
    pathMatch: "full",
    component: RequestsComponent,
    canActivate: [RoleGuard],
    data:{expectedRoles: 'MANAGER'}
  },
  {
    path: "requests/:type/:id",
    pathMatch: "full",
    component: RequestDetaliesComponent,
    canActivate: [RoleGuard],
    data:{expectedRoles: 'MANAGER'}
  },
  {
    path: "incomeReports",
    pathMatch: "full",
    component: IncomeReportsComponent,
    canActivate: [RoleGuard],
    data:{expectedRoles: 'MANAGER|OWNER'}
  },
  {
    path: "employeeReports",
    pathMatch: "full",
    component: EmployeeReportsComponent,
    canActivate: [RoleGuard],
    data:{expectedRoles: 'MANAGER|OWNER'}
  },
  {
    path: "articleReports",
    pathMatch: "full",
    component: ArticleReportsComponent,
    canActivate: [RoleGuard],
    data:{expectedRoles: 'MANAGER|OWNER'}
  },

];
