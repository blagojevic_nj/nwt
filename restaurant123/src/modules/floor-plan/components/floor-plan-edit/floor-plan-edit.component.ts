import { Component, OnInit } from '@angular/core';
import { FloorPlanService } from '../../services/floor-plan.service';
import { PositionedTableElement } from 'src/modules/shared/models/floor-plan/positioned-table-element';

@Component({
  selector: 'app-floor-plan-edit',
  templateUrl: './floor-plan-edit.component.html',
  styleUrls: ['./floor-plan-edit.component.scss']
})
export class FloorPlanEditComponent implements OnInit {

  tables: PositionedTableElement[] = new Array();

  constructor(private floorPlanService: FloorPlanService) { }

  ngOnInit(): void {
    this.floorPlanService.getFloorPlan().subscribe(res => {this.tables = res})
    
  }

}
