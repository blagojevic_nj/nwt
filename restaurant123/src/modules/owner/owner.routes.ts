import { Routes } from "@angular/router";
import { RoleGuard } from "../auth/guards/role/role.guard";
import { OwnerArticlesReportComponent } from "./components/owner-articles-report/owner-articles-report.component";
import { OwnerEmployeeReportComponent } from "./components/owner-employee-report/owner-employee-report.component";
import { OwnerFinancialReportComponent } from "./components/owner-financial-report/owner-financial-report.component";
import { OwnerHomePageComponent } from "./components/owner-home-page/owner-home-page.component";

export const OwnerRoutes: Routes = [
  {
    path: "",
    pathMatch: "full",
    component: OwnerHomePageComponent,
    canActivate: [RoleGuard],
    data:{expectedRoles: 'OWNER'}
  },

  {
    path: "incomeReports",
    pathMatch: "full",
    component: OwnerFinancialReportComponent,
    canActivate: [RoleGuard],
    data:{expectedRoles: 'OWNER'}
  },
  {
    path: "employeeReports",
    pathMatch: "full",
    component: OwnerEmployeeReportComponent,
    canActivate: [RoleGuard],
    data:{expectedRoles: 'OWNER'}
  },
  {
    path: "articleReports",
    pathMatch: "full",
    component: OwnerArticlesReportComponent,
    canActivate: [RoleGuard],
    data:{expectedRoles: 'OWNER'}
  },

];
