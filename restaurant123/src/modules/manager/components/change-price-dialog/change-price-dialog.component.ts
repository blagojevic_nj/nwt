import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import PriceChangeData from 'src/modules/shared/models/manager/price-change-data';
import { ManagerService } from '../../services/manager.service';

@Component({
  selector: 'app-change-price-dialog',
  templateUrl: './change-price-dialog.component.html',
  styleUrls: ['./change-price-dialog.component.scss']
})
export class ChangePriceDialogComponent implements OnInit {

  id: number = -1;
  oldPrice: number = 0;
  form: FormGroup;

  constructor(
  @Inject(MAT_DIALOG_DATA) public data: PriceChangeData,
  private toastr: ToastrService,
  private managerService: ManagerService,
  public matDialogRef: MatDialogRef<ChangePriceDialogComponent>,
  private formBuilder: FormBuilder
  ){
    this.form = this.formBuilder.group({
      novaCena: [null, Validators.required]
    });
   }

  ngOnInit(): void {
    this.id = this.data.id;
    this.oldPrice = this.data.oldPrice;
  }


  submit(): void {
    if(! this.form.valid || this.form.value.novaCena <0 ){
      this.toastr.error("Nova cena nije validan broj!")
      return
    }

    if(this.form.value.novaCena == this.oldPrice)
    {
      this.toastr.warning("Cena artikla je već " + this.oldPrice + " rsd.")
      this.matDialogRef.close(null);
      return
    }
    
    this.managerService.setNewPrice(this.id, this.form.value.novaCena)
    .subscribe(res => {
      if(res){
        this.toastr.success("Uspešna izmena cene!")
        this.matDialogRef.close(res);
        return
      }
      else{
      this.toastr.error("Greška, pokušajte ponovo.")
      this.matDialogRef.close(null);
      return
      }
    })

  }

}
